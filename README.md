# README #
## Game Manager - ASPNET Core 3.1  ASP.NET Core 3.1 / Angular 9 / Razor pages/ project template ##

 Modelo de projeto de inicialização com gerenciamento de emprestimo de jogos, amigos e autenticação com roles.
 Este modelo de aplicativo fornece uma maneira rápida e conveniente de criar aplicativos ASP.NET Core 3.1 / Angular 9 / Razor pages / xamarin forms. 
 Além de outros serviços úteis para desenvolvimento rápido de aplicativos. 



## Este aplicativo consiste em: ##
1. Uma estrutura de projeto API ASP.NET CORE de back-end HTTP RESTful controlador de serviços, para gerenciamento de token com usuário e roles.[API AUTHENTICATION](https://gamemanager.auth.hcodetecnologia.com.br/swagger/index.html)
2. Uma estrutura de projeto API ASP.NET CORE de back-end HTTP RESTful controlador de serviços, para gerenciamento de jogos, amigos e empréstimos com  integração de login, usuário e permissão. [API GERENCIADOR DE JOGOS](https://gamemanager.api.hcodetecnologia.com.br/swagger/index.html)           
3. Uma estrutura de projeto Aplicativo Web ASP.NET CORE de front-end com conteudo Razor Pages, com integração de  gerenciamento de jogos, amigos e empréstimos, com autenticação usuário e permissão. [GAME MANAGER-RAZOR](https://managergame.hcodetecnologia.com.br)                 
4. Uma estrutura de projeto Aplicativo Web ASP.NET CORE de front-end com conteudo Angular 9, com integração de  gerenciamento de jogos, amigos e empréstimos, com autenticação usuário e permissão. [GAME MANAGER-ANGULA](https://gamemanager.hcodetecnologia.com.br)
  

### Features ###
* Framework 3.1 do .Net Core.
* Clean and simple Angular/ASP.NET Core code to serve as a guide
* Clean and simple Razor/ASP.NET Core code to serve as a guide
* Data Access Layer built with the Repository and Unit of Work Pattern
* Code First Database
* A RESTful API Design
* Dialog and Notification Services
* Handling Access Tokens (Bearer authentication)
* CRUD APIs
* DDD (Domain Driven Design) and IoC
* etc.



### Technologies ###
* Template pages with Angular 9 and TypeScript
* Template pages with Razor pages
* RESTful API Backend using ASP.NET Core 3.1 Web API
* Database using Entity Framework Core
* Authentication based on Introduction to JSON Web Tokens
* API Documentation using Swagger
* Client-side built on Angular CLI
* Theming using Bootstrap 4 with Bootswatch
* Docker compose


### Instalação ###
* [Clone o git clone https://bitbucket.org/wcaraujo/webapijogo.git](https://bitbucket.org/wcaraujo/webapijogo.git) e edite com seu editor favorito. e.g. Visual Studio, Visual Studio Code.

### Login ###
* Username = "batman", Password = "batman", Role = "manager"
* Username = "robin", Password = "robin", Role = "employee"