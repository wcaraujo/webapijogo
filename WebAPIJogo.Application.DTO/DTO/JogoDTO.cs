﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

namespace WebAPIJogo.Application.DTO.DTO
{
    public class JogoDTO
    {
        #region property
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Categoria { get; set; }
        public bool Ativo { get; set; }
        public bool Disponivel { get; set; }
        #endregion
    }
}
