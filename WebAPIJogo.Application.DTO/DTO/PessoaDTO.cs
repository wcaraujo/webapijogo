﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System.Collections.Generic;

namespace WebAPIJogo.Application.DTO.DTO
{
    public class PessoaDTO
    {

        #region property
        public int? Id { get; set; }
        public string Nome { get; set; }
        public virtual List<EmprestimoDTO> EmprestimosDTO { get; set; } = new List<EmprestimoDTO>();
        #endregion

    }
}
