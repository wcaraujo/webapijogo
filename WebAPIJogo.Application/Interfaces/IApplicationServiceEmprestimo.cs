﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Domain.Core.Interfaces.Services.Base;

namespace WebAPIJogo.Application.Interfaces
{
    public interface IApplicationServiceEmprestimo : IServiceBase<EmprestimoDTO>
    {

        #region methods opcionais
        Task<IEnumerable<EmprestimoDTO>> GetByJogo(int JogoId);
        Task<IEnumerable<EmprestimoDTO>> GetByPessoa(int PessoaId);
        Task<EmprestimoDTO> GetByPessoaJogo(int PessoaId, int JogoId);
        Task<bool> FinalizarEmprestimo(Guid id);
        #endregion
    }
}
