﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System.Threading.Tasks;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Domain.Core.Interfaces.Services.Base;

namespace WebAPIJogo.Application.Interfaces
{
    public interface IApplicationServiceJogo : IServiceBase<JogoDTO>
    {
        #region methods
        #endregion
    }
}
