﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System.Threading.Tasks;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Domain.Core.Interfaces.Services.Base;
using WebAPIJogo.Domain.Models;

namespace WebAPIJogo.Application.Interfaces
{
    public interface IApplicationServicePessoa : IServiceBase<PessoaDTO>
    {
        #region methods opcionais
        Task<PessoaDTO> GetEmprestimoPendente(int key);
        #endregion
    }
}
