﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Application.Interfaces;
using WebAPIJogo.Domain.Core.Interfaces.Services;
using WebAPIJogo.Infra.CrossCutting.Adapter.Interfaces;

namespace WebAPIJogo.Application.Services
{
    public class ApplicationServiceEmprestimo : IApplicationServiceEmprestimo
    {
        #region property
        private readonly IServiceEmprestimo _serviceEmprestimo;
        private readonly IMapperEmprestimo _mapperEmprestimo;
        #endregion


        #region Constructor
        public ApplicationServiceEmprestimo(IServiceEmprestimo serviceEmprestimo, IMapperEmprestimo mapperEmprestimo)
        {
            _serviceEmprestimo = serviceEmprestimo;
            _mapperEmprestimo = mapperEmprestimo;
        }
        #endregion


        #region methods
        public void Create(EmprestimoDTO obj)
        {
            var emprestimo = _mapperEmprestimo.MapperToEntity(obj);
            _serviceEmprestimo.Create(emprestimo);
        }
        public void Delete(EmprestimoDTO obj)
        {
            var emprestimo = _mapperEmprestimo.MapperToEntity(obj);
            _serviceEmprestimo.Delete(emprestimo);
        }
        public void Dispose()
        {
            _serviceEmprestimo.Dispose();
        }
        public async Task<EmprestimoDTO> FindByKeyAsync(object key)
        {
            var emprestimo = await _serviceEmprestimo.FindByKeyAsync(key);
            return _mapperEmprestimo.MapperToDTO(emprestimo);
        }
        public async Task<IEnumerable<EmprestimoDTO>> GetAllAsync()
        {
            var emprestimos = await _serviceEmprestimo.GetAllAsync();
            return _mapperEmprestimo.MapperListEmprestimos(emprestimos);
        }
        public async Task<IEnumerable<EmprestimoDTO>> GetByJogo(int JogoId)
        {
            var emprestimos = await _serviceEmprestimo.GetByJogo(JogoId);
            return _mapperEmprestimo.MapperListEmprestimos(emprestimos);
        }
        public async Task<IEnumerable<EmprestimoDTO>> GetByPessoa(int PessoaId)
        {
            var emprestimos = await _serviceEmprestimo.GetByPessoa(PessoaId);
            return _mapperEmprestimo.MapperListEmprestimos(emprestimos);
        }
        public async Task<EmprestimoDTO> GetByPessoaJogo(int PessoaId, int JogoId)
        {
            var emprestimos = await _serviceEmprestimo.GetByPessoaJogo(PessoaId, JogoId);
            return _mapperEmprestimo.MapperToDTO(emprestimos);
        }
        public void Update(EmprestimoDTO obj)
        {
            var emprestimo = _mapperEmprestimo.MapperToEntity(obj);
            _serviceEmprestimo.Update(emprestimo);
        }


        public async Task<bool> FinalizarEmprestimo(Guid id)
        {

            return await _serviceEmprestimo.FinalizarEmprestimo(id);
        }
        #endregion
    }
}
