﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Application.Interfaces;
using WebAPIJogo.Domain.Core.Interfaces.Services;
using WebAPIJogo.Infra.CrossCutting.Adapter.Interfaces;

namespace WebAPIJogo.Application.Services
{
    public class ApplicationServiceJogo : IApplicationServiceJogo
    {
        #region property
        private readonly IServiceJogo _serviceJogo;
        private readonly IMapperJogo _mapperJogo;
        #endregion


        #region Constructor
        public ApplicationServiceJogo(IServiceJogo serviceJogo, IMapperJogo mapperJogo)
        {
            _serviceJogo = serviceJogo;
            _mapperJogo = mapperJogo;
        }
        #endregion


        #region methods
        public void Create(JogoDTO obj)
        {
            var objJogo = _mapperJogo.MapperToEntity(obj);
            _serviceJogo.Create(objJogo);
        }
        public void Delete(JogoDTO obj)
        {
            var objJogo = _mapperJogo.MapperToEntity(obj);
            _serviceJogo.Delete(objJogo);
        }
        public void Dispose()
        {
            _serviceJogo.Dispose();
        }
        public async Task<JogoDTO> FindByKeyAsync(object id)
        {
            var objJogo = await _serviceJogo.FindByKeyAsync(id);
            return _mapperJogo.MapperToDTO(objJogo);
        }
        public async Task<IEnumerable<JogoDTO>> GetAllAsync()
        {
            var objJogos = await _serviceJogo.GetAllAsync();
            return _mapperJogo.MapperListJogos(objJogos);
        }

        //public async Task<bool> IsDisponivel(int id)
        //{
        //    var jogo = await _serviceJogo.FindByKeyAsync(id);
        //    return jogo.Disponivel;
        //}

        public void Update(JogoDTO obj)
        {
            var objJogo = _mapperJogo.MapperToEntity(obj);
            _serviceJogo.Update(objJogo);
        }
        #endregion
    }
}
