﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Application.Interfaces;
using WebAPIJogo.Domain.Core.Interfaces.Services;
using WebAPIJogo.Domain.Models;
using WebAPIJogo.Infra.CrossCutting.Adapter.Interfaces;

namespace WebAPIJogo.Application.Services
{
    public class ApplicationServicePessoa : IApplicationServicePessoa
    {
        #region property
        private readonly IServicePessoa _servicePessoa;
        private readonly IMapperPessoa _mapperPessoa;
        #endregion


        #region Constructor
        public ApplicationServicePessoa(IServicePessoa servicePessoa, IMapperPessoa mapperPessoa)
        {
            _servicePessoa = servicePessoa;
            _mapperPessoa = mapperPessoa;
        }
        #endregion


        #region methods
        public void Create(PessoaDTO obj)
        {
            var objPessoa = _mapperPessoa.MapperToEntity(obj);
            _servicePessoa.Create(objPessoa);
        }

        public void Delete(PessoaDTO obj)
        {
            var objPessoa = _mapperPessoa.MapperToEntity(obj);
            _servicePessoa.Delete(objPessoa);
        }

        public void Dispose()
        {
            _servicePessoa.Dispose();
        }

        public async Task<PessoaDTO> FindByKeyAsync(object id)
        {
            var objPessoa = await _servicePessoa.FindByKeyAsync(id);
            return _mapperPessoa.MapperToDTO(objPessoa);
        }

        public async Task<IEnumerable<PessoaDTO>> GetAllAsync()
        {
            var objPessoas = await _servicePessoa.GetAllAsync();
            return _mapperPessoa.MapperListPessoas(objPessoas);
        }

        public async Task<PessoaDTO> GetEmprestimoPendente(int key)
        {
            var pessoa = await _servicePessoa.GetEmprestimoPendente(key);
            return _mapperPessoa.MapperToDTO(pessoa);
        }

        public void Update(PessoaDTO obj)
        {
            var objPessoa = _mapperPessoa.MapperToEntity(obj);
            _servicePessoa.Update(objPessoa);
        }
        #endregion
    }
}
