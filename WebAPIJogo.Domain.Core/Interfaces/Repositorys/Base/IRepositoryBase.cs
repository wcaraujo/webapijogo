﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WebAPIJogo.Domain.Core.Interfaces.Repositorys.Base
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {

        #region methods
        void Create(TEntity entity);
        Task<TEntity> FindByKeyAsync(object key);
        Task<IEnumerable<TEntity>> GetAllAsync();
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Dispose();
        #endregion
    }
}
