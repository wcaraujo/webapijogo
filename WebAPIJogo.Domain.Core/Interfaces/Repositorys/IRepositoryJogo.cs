﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using WebAPIJogo.Domain.Core.Interfaces.Repositorys.Base;
using WebAPIJogo.Domain.Models;

namespace WebAPIJogo.Domain.Core.Interfaces.Repositorys
{
    public interface IRepositoryJogo : IRepositoryBase<Jogo>
    {
    }
}
