﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System.Threading.Tasks;
using WebAPIJogo.Domain.Core.Interfaces.Repositorys.Base;
using WebAPIJogo.Domain.Models;

namespace WebAPIJogo.Domain.Core.Interfaces.Repositorys
{
    public interface IRepositoryPessoa : IRepositoryBase<Pessoa>
    {
        Task<Pessoa> GetEmprestimoPendente(int key);
    }
}
