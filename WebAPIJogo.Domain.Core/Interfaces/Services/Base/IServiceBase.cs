﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Domain.Core.Interfaces.Repositorys.Base;

namespace WebAPIJogo.Domain.Core.Interfaces.Services.Base
{
    //public interface IServiceBase<TEntity> where TEntity : class
    //{
    //    #region methods
    //    void Create(TEntity entity);
    //    Task<TEntity> FindByKeyAsync(object key);
    //    Task<IEnumerable<TEntity>> GetAllAsync();
    //    void Update(TEntity entity);
    //    void Delete(TEntity entity);
    //    void Dispose();
    //    #endregion
    //}

    public interface IServiceBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    { }


}
