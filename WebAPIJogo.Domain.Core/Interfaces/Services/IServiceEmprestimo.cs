﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Domain.Core.Interfaces.Services.Base;
using WebAPIJogo.Domain.Models;

namespace WebAPIJogo.Domain.Core.Interfaces.Services
{
    public interface IServiceEmprestimo : IServiceBase<Emprestimo>
    {
        Task<IEnumerable<Emprestimo>> GetByJogo(int JogoId);
        Task<IEnumerable<Emprestimo>> GetByPessoa(int PessoaId);
        Task<Emprestimo> GetByPessoaJogo(int PessoaId, int JogoId);
        Task<bool> FinalizarEmprestimo(Guid id);
    }
}
