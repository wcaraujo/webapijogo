﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================



using System.Threading.Tasks;
using WebAPIJogo.Domain.Core.Interfaces.Services.Base;
using WebAPIJogo.Domain.Models;

namespace WebAPIJogo.Domain.Core.Interfaces.Services
{
    public interface IServicePessoa : IServiceBase<Pessoa>
    {
        Task<Pessoa> GetEmprestimoPendente(int key);
    }
}
