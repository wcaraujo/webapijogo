﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebAPIJogo.Domain.Core.Interfaces.Repositorys.Base;
using WebAPIJogo.Domain.Core.Interfaces.Services.Base;

namespace WebAPIJogo.Domain.Service.Services.Base
{
    public abstract class ServiceBase<TEntity> : IDisposable, IServiceBase<TEntity> where TEntity : class
    {

        #region property 
        private readonly IRepositoryBase<TEntity> _repository;
        #endregion


        #region Constructor
        protected ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }
        #endregion


        #region methods
        public void Dispose()
        {
            _repository.Dispose();
        }
        public async Task<TEntity> FindByKeyAsync(object key)
        {
            return await _repository.FindByKeyAsync(key);
        }
        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _repository.GetAllAsync();
        }
        public void Update(TEntity entity)
        {
            _repository.Update(entity);
        }
        public void Create(TEntity entity)
        {
            _repository.Create(entity);
        }
        public void Delete(TEntity entity)
        {
            _repository.Delete(entity);
        }
        #endregion
    }
}
