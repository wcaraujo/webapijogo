﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Domain.Core.Interfaces.Repositorys;
using WebAPIJogo.Domain.Core.Interfaces.Services;
using WebAPIJogo.Domain.Models;
using WebAPIJogo.Domain.Service.Services.Base;

namespace WebAPIJogo.Domain.Service.Services
{
    public class ServiceEmprestimo : ServiceBase<Emprestimo>, IServiceEmprestimo
    {
        private readonly IRepositoryEmprestimo _repositoryEmprestimo;

        #region Constructor
        public ServiceEmprestimo(IRepositoryEmprestimo repositoryEmprestimo) : base(repositoryEmprestimo)
        {
            _repositoryEmprestimo = repositoryEmprestimo;
        }
        #endregion

        #region methods opcionais    
        public async Task<IEnumerable<Emprestimo>> GetByJogo(int JogoId)
        {
            return await _repositoryEmprestimo.GetByJogo(JogoId);
        }
        public async Task<IEnumerable<Emprestimo>> GetByPessoa(int PessoaId)
        {
            return await _repositoryEmprestimo.GetByPessoa(PessoaId);
        }
        public async Task<Emprestimo> GetByPessoaJogo(int PessoaId, int JogoId)
        {
            return await _repositoryEmprestimo.GetByPessoaJogo(PessoaId, JogoId);
        }
        public async Task<bool> FinalizarEmprestimo(Guid id) { return await _repositoryEmprestimo.FinalizarEmprestimo(id); }
        #endregion
    }
}
