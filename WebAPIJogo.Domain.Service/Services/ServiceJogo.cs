﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using WebAPIJogo.Domain.Core.Interfaces.Repositorys;
using WebAPIJogo.Domain.Core.Interfaces.Services;
using WebAPIJogo.Domain.Models;
using WebAPIJogo.Domain.Service.Services.Base;

namespace WebAPIJogo.Domain.Service.Services
{
    public class ServiceJogo : ServiceBase<Jogo>, IServiceJogo
    {
        #region Constructor
        public ServiceJogo(IRepositoryJogo repositoryJogo) : base(repositoryJogo)
        {
        }
        #endregion      
    }
}
