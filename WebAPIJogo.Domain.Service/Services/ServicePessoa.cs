﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================



using System.Threading.Tasks;
using WebAPIJogo.Domain.Core.Interfaces.Repositorys;
using WebAPIJogo.Domain.Core.Interfaces.Services;
using WebAPIJogo.Domain.Models;
using WebAPIJogo.Domain.Service.Services.Base;

namespace WebAPIJogo.Domain.Service.Services
{
    public class ServicePessoa : ServiceBase<Pessoa>, IServicePessoa
    {
        private readonly IRepositoryPessoa _repositoryPessoa;

        #region Constructor
        public ServicePessoa(IRepositoryPessoa repositoryPessoa) : base(repositoryPessoa)
        {
            _repositoryPessoa = repositoryPessoa;
        }
        #endregion

        #region methods opcionais  
        public Task<Pessoa> GetEmprestimoPendente(int key)
        {
            return _repositoryPessoa.GetEmprestimoPendente(key);
        }
        #endregion
    }
}
