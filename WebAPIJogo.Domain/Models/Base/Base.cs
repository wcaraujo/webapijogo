﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System;
namespace WebAPIJogo.Domain.Models.Base
{
    public abstract class BaseEntity : BaseAuditableEntity
    {
        #region property
        public int? Id { get; set; }  
        #endregion
    }
}
