﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System;
using WebAPIJogo.Domain.Interfaces;

namespace WebAPIJogo.Domain.Models.Base
{
    public abstract class BaseAuditableEntity : IBaseAuditableEntity
    {

        #region property
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        #endregion
    }
}
