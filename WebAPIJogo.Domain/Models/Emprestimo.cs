﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System;
using System.Collections.Generic;

namespace WebAPIJogo.Domain.Models
{
    public class Emprestimo
    {
        #region property
        public Guid Id { get; set; }
        public int PessoaId { get; set; }
        public virtual Pessoa Amigo { get; set; }
        public int JogoId { get; set; }
        public virtual Jogo Jogo { get; set; }
        public DateTime DataEmprestimo { get; set; }
        public DateTime DataDevolucao { get; set; }
        public DateTime DataEntrega { get; set; }
        public bool Entregue { get; set; }
        #endregion
    }
}
