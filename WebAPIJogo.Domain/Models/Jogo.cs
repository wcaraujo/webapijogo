﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using WebAPIJogo.Domain.Models.Base;
namespace WebAPIJogo.Domain.Models
{
    public class Jogo : BaseEntity
    {
        #region property
        public string Name { get; set; }
        public string Categoria { get; set; }
        public bool Ativo { get; set; }
        public bool Disponivel { get; set; }
        #endregion
    }
}
