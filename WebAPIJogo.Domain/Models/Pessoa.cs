﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System.Collections.Generic;
using WebAPIJogo.Domain.Models.Base;

namespace WebAPIJogo.Domain.Models
{
    public class Pessoa : BaseEntity
    {
        #region property
        public string Nome { get; set; }
        public virtual ICollection<Emprestimo> Emprestimos { get; set; }
        #endregion
    }

}
