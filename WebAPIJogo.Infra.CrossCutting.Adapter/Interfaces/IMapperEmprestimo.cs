﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System.Collections.Generic;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Domain.Models;

namespace WebAPIJogo.Infra.CrossCutting.Adapter.Interfaces
{
    public interface IMapperEmprestimo
    {
        #region Mappers
        Emprestimo MapperToEntity(EmprestimoDTO emprestimoDTO);
        List<EmprestimoDTO> MapperListEmprestimos(IEnumerable<Emprestimo>  emprestimos);
        EmprestimoDTO MapperToDTO(Emprestimo emprestimo);
        #endregion
    }
}
