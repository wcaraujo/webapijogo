﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System.Collections.Generic;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Domain.Models;

namespace WebAPIJogo.Infra.CrossCutting.Adapter.Interfaces
{
    public interface IMapperJogo
    {
        #region Mappers
        Jogo MapperToEntity(JogoDTO jogoDTO);
        List<JogoDTO> MapperListJogos(IEnumerable<Jogo> jogos);
        JogoDTO MapperToDTO(Jogo jogo);
        #endregion
    }
}
