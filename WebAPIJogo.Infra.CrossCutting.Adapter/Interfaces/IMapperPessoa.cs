﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System.Collections.Generic;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Domain.Models;

namespace WebAPIJogo.Infra.CrossCutting.Adapter.Interfaces
{
    public interface IMapperPessoa
    {
        #region Mappers
        Pessoa MapperToEntity(PessoaDTO pessoaDTO);
        List<PessoaDTO> MapperListPessoas(IEnumerable<Pessoa> pessoas);
        PessoaDTO MapperToDTO(Pessoa pessoa);
        #endregion
    }
}
