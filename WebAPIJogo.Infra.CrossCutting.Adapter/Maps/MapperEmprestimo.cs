﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System.Collections.Generic;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Domain.Models;
using WebAPIJogo.Infra.CrossCutting.Adapter.Interfaces;

namespace WebAPIJogo.Infra.CrossCutting.Adapter.Maps
{
    public class MapperEmprestimo : IMapperEmprestimo
    {
        #region property
        private readonly List<EmprestimoDTO> EmprestimosDTO = new List<EmprestimoDTO>();
        #endregion


        #region methods
        public List<EmprestimoDTO> MapperListEmprestimos(IEnumerable<Emprestimo> emprestimos)
        {
            foreach (var item in emprestimos)
            {
                EmprestimoDTO emprestimoDTO = new EmprestimoDTO
                {
                    Id = item.Id,
                    DataDevolucao = item.DataDevolucao,
                    DataEmprestimo = item.DataEmprestimo,
                    DataEntrega = item.DataEntrega,
                    Entregue = item.Entregue
                };
                if (item.Amigo != null)
                {
                    emprestimoDTO.AmigoDTO = new PessoaDTO
                    {
                        Id = item.Amigo.Id,
                        Nome = item.Amigo.Nome,
                        EmprestimosDTO = null
                    };
                }
                if (item.Jogo != null)
                {
                    emprestimoDTO.JogoDTO = new JogoDTO()
                    {
                        Ativo = item.Jogo.Ativo,
                        Categoria = item?.Jogo.Categoria,
                        Disponivel = item.Jogo.Disponivel,
                        Id = item?.Jogo.Id,
                        Name = item.Jogo.Name
                    };
                }
                EmprestimosDTO.Add(emprestimoDTO);
            }
            return EmprestimosDTO;
        }
        public EmprestimoDTO MapperToDTO(Emprestimo emprestimo)
        {
            EmprestimoDTO emprestimoDTO = null;
            if (emprestimo != null)
            {
                emprestimoDTO = new EmprestimoDTO()
                {
                    Id = emprestimo.Id,
                    DataDevolucao = emprestimo.DataDevolucao,
                    DataEmprestimo = emprestimo.DataEmprestimo,
                    DataEntrega = emprestimo.DataEntrega,
                    Entregue = emprestimo.Entregue
                };
                if (emprestimo.Amigo != null)
                {
                    emprestimoDTO.AmigoDTO = new PessoaDTO
                    {
                        Id = emprestimo.Amigo.Id,
                        Nome = emprestimo.Amigo.Nome,
                        EmprestimosDTO = null

                    };
                }
                if (emprestimo.Jogo != null)
                {
                    emprestimoDTO.JogoDTO = new JogoDTO()
                    {
                        Ativo = emprestimo.Jogo.Ativo,
                        Categoria = emprestimo.Jogo.Categoria,
                        Disponivel = emprestimo.Jogo.Disponivel,
                        Id = emprestimo.Jogo.Id,
                        Name = emprestimo.Jogo.Name
                    };
                }

            }
            return emprestimoDTO;
        }
        public Emprestimo MapperToEntity(EmprestimoDTO emprestimoDTO)
        {
            Emprestimo emprestimo = null;
            if (emprestimoDTO != null)
            {
                emprestimo = new Emprestimo
                {
                    Id = emprestimoDTO.Id,
                    DataDevolucao = emprestimoDTO.DataDevolucao,
                    DataEmprestimo = emprestimoDTO.DataEmprestimo,
                    DataEntrega = emprestimoDTO.DataEntrega,
                    Entregue = emprestimoDTO.Entregue
                };
                if (emprestimoDTO.AmigoDTO != null)
                {

                    emprestimo.PessoaId = (int)emprestimoDTO.AmigoDTO.Id;


                    emprestimo.Amigo = new Pessoa
                    {
                        Id = emprestimoDTO.AmigoDTO.Id,
                        Nome = emprestimoDTO.AmigoDTO.Nome
                    };

                }
                if (emprestimoDTO.JogoDTO != null)
                {
                    emprestimo.JogoId = (int)emprestimoDTO.JogoDTO.Id;

                    emprestimo.Jogo = new Jogo()
                    {
                        Ativo = emprestimoDTO.JogoDTO.Ativo,
                        Categoria = emprestimoDTO.JogoDTO.Categoria,
                        Disponivel = emprestimoDTO.JogoDTO.Disponivel,
                        Id = emprestimoDTO.JogoDTO.Id,
                        Name = emprestimoDTO.JogoDTO.Name
                    };
                }
            }
            return emprestimo;
        }
        #endregion
    }
}
