﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System;
using System.Collections.Generic;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Domain.Models;
using WebAPIJogo.Infra.CrossCutting.Adapter.Interfaces;

namespace WebAPIJogo.Infra.CrossCutting.Adapter.Maps
{
    public class MapperJogo : IMapperJogo
    {
        #region property
        public readonly List<JogoDTO> JogosDTO = new List<JogoDTO>();
        #endregion


        #region methods
        public List<JogoDTO> MapperListJogos(IEnumerable<Jogo> jogos)
        {
            foreach (var item in jogos)
            {
                JogoDTO jogoDTO = new JogoDTO
                {
                    Id = item.Id,
                    Name = item.Name,
                    Ativo = item.Ativo,
                    Categoria = item.Categoria,
                    Disponivel = item.Disponivel
                };
                JogosDTO.Add(jogoDTO);
            }

            return JogosDTO;
        }
        public JogoDTO MapperToDTO(Jogo jogo)
        {
            JogoDTO jogoDTO = null;
            if (jogo != null)
            {
                jogoDTO = new JogoDTO()
                {
                    Id = jogo.Id,
                    Name = jogo.Name,
                    Ativo = jogo.Ativo,
                    Categoria = jogo.Categoria,
                    Disponivel = jogo.Disponivel
                };
            }


            return jogoDTO;
        }
        public Jogo MapperToEntity(JogoDTO jogoDTO)
        {
            Jogo jogo = null;
            if (jogoDTO != null)
            {
                jogo = new Jogo
                {
                    Id = jogoDTO.Id,
                    Name = jogoDTO.Name,
                    Categoria = jogoDTO.Categoria,
                    Ativo = jogoDTO.Ativo,
                    Disponivel = jogoDTO.Disponivel
                };
            }
            return jogo;
        }
        #endregion
    }
}
