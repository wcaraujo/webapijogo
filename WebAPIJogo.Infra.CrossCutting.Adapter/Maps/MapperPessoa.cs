﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System.Collections.Generic;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Domain.Models;
using WebAPIJogo.Infra.CrossCutting.Adapter.Interfaces;

namespace WebAPIJogo.Infra.CrossCutting.Adapter.Maps
{
    public class MapperPessoa : IMapperPessoa
    {

        #region property
        public readonly List<PessoaDTO> PessoasDTO = new List<PessoaDTO>();
        #endregion


        #region methods
        public List<PessoaDTO> MapperListPessoas(IEnumerable<Pessoa> pessoas)
        {
            foreach (var item in pessoas)
            {
                PessoaDTO pessoaDTO = new PessoaDTO
                {
                    Id = item.Id,
                    Nome = item.Nome
                };

                if (item.Emprestimos != null)
                {

                    foreach (var emprestimo in item.Emprestimos)
                    {
                        EmprestimoDTO emprestimoDTO = new EmprestimoDTO
                        {
                            Id = emprestimo.Id,
                            DataDevolucao = emprestimo.DataDevolucao,
                            DataEmprestimo = emprestimo.DataEmprestimo,
                            DataEntrega = emprestimo.DataEntrega,
                            Entregue = emprestimo.Entregue,
                            JogoDTO = new JogoDTO { Ativo = emprestimo.Jogo.Ativo, Categoria = emprestimo.Jogo.Categoria, Disponivel = emprestimo.Jogo.Disponivel, Id = emprestimo.Jogo.Id, Name = emprestimo.Jogo.Name }
                        };
                        pessoaDTO.EmprestimosDTO.Add(emprestimoDTO);
                    }
                    PessoasDTO.Add(pessoaDTO);

                }

            }
            return PessoasDTO;
        }
        public PessoaDTO MapperToDTO(Pessoa pessoa)
        {
            PessoaDTO pessoaDTO = null;
            if (pessoa != null)
            {
                pessoaDTO = new PessoaDTO
                {
                    Id = pessoa.Id,
                    Nome = pessoa.Nome
                };

                if (pessoa.Emprestimos != null)
                {
                    foreach (var emprestimo in pessoa.Emprestimos)
                    {
                        EmprestimoDTO emprestimoDTO = new EmprestimoDTO
                        {
                            Id = emprestimo.Id,
                            DataDevolucao = emprestimo.DataDevolucao,
                            DataEmprestimo = emprestimo.DataEmprestimo,
                            DataEntrega = emprestimo.DataEntrega,
                            Entregue = emprestimo.Entregue,
                            JogoDTO = new JogoDTO { Ativo = emprestimo.Jogo.Ativo, Categoria = emprestimo.Jogo.Categoria, Disponivel = emprestimo.Jogo.Disponivel, Id = emprestimo.Jogo.Id, Name = emprestimo.Jogo.Name }
                        };
                        pessoaDTO.EmprestimosDTO.Add(emprestimoDTO);
                    }
                }
            }

            return pessoaDTO;
        }
        public Pessoa MapperToEntity(PessoaDTO pessoaDTO)
        {
            Pessoa pessoa = null;
            if (pessoaDTO != null)
            {
                pessoa = new Pessoa
                {
                    Id = pessoaDTO.Id,
                    Nome = pessoaDTO.Nome,
                };
                if (pessoaDTO.EmprestimosDTO != null)
                {

                    foreach (var emprestimoDTO in pessoaDTO.EmprestimosDTO)
                    {
                        Emprestimo emprestimo = new Emprestimo
                        {
                            Id = emprestimoDTO.Id,
                            DataDevolucao = emprestimoDTO.DataDevolucao,
                            DataEmprestimo = emprestimoDTO.DataEmprestimo,
                            DataEntrega = emprestimoDTO.DataEntrega,
                            Entregue = emprestimoDTO.Entregue,
                            Jogo = new Jogo { Ativo = emprestimoDTO.JogoDTO.Ativo, Categoria = emprestimoDTO.JogoDTO.Categoria, Disponivel = emprestimoDTO.JogoDTO.Disponivel, Id = emprestimoDTO.JogoDTO.Id, Name = emprestimoDTO.JogoDTO.Name }
                        };
                        pessoa.Emprestimos.Add(emprestimo);
                    }
                }
            }
            return pessoa;
        }
        #endregion

    }
}
