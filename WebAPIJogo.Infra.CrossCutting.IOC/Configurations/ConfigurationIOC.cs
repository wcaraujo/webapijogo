﻿using Autofac;
using WebAPIJogo.Application.Interfaces;
using WebAPIJogo.Application.Services;
using WebAPIJogo.Domain.Core.Interfaces.Repositorys;
using WebAPIJogo.Domain.Core.Interfaces.Services;
using WebAPIJogo.Domain.Service.Services;
using WebAPIJogo.Infra.CrossCutting.Adapter.Interfaces;
using WebAPIJogo.Infra.CrossCutting.Adapter.Maps;
using WebAPIJogo.Infra.Data.Repository.Repositorys;

namespace WebAPIJogo.Infra.CrossCutting.IOC.Configurations
{
    public class ConfigurationIOC
    {
        public static void Load(ContainerBuilder builder)
        {
            #region Registra IOC

            #region IOC Application
            builder.RegisterType<ApplicationServiceJogo>().As<IApplicationServiceJogo>();
            builder.RegisterType<ApplicationServicePessoa>().As<IApplicationServicePessoa>();
            builder.RegisterType<ApplicationServiceEmprestimo>().As<IApplicationServiceEmprestimo>();         
            #endregion

            #region IOC Services
            builder.RegisterType<ServiceJogo>().As<IServiceJogo>();
            builder.RegisterType<ServicePessoa>().As<IServicePessoa>();
            builder.RegisterType<ServiceEmprestimo>().As<IServiceEmprestimo>();
            #endregion

            #region IOC Repositorys SQL
            builder.RegisterType<RepositoryJogo>().As<IRepositoryJogo>();
            builder.RegisterType<RepositoryPessoa>().As<IRepositoryPessoa>();
            builder.RegisterType<RepositoryEmprestimo>().As<IRepositoryEmprestimo>();
            #endregion

            #region IOC Mapper
            builder.RegisterType<MapperJogo>().As<IMapperJogo>();
            builder.RegisterType<MapperPessoa>().As<IMapperPessoa>();
            builder.RegisterType<MapperEmprestimo>().As<IMapperEmprestimo>();
            #endregion

            #endregion

        }
    }
}
