﻿using Autofac;
using WebAPIJogo.Infra.CrossCutting.IOC.Configurations;

namespace WebAPIJogo.Infra.CrossCutting.IOC.Modules
{
    public class ModuleIOC : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            #region Carrega IOC
            ConfigurationIOC.Load(builder);
            #endregion
        }
    }
}
