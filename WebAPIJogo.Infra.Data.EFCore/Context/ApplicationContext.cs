﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebAPIJogo.Domain.Interfaces;
using WebAPIJogo.Domain.Models;


namespace WebAPIJogo.Infra.Data.EFCore.Context
{
    public class ApplicationContext : DbContext
    {
        public string CurrentUserId { get; set; }

        public DbSet<Jogo> Jogos { get; set; }
        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet<Emprestimo> Emprestimos { get; set; }

        public ApplicationContext(DbContextOptions options) : base(options)
        { }


        public override int SaveChanges()
        {
            UpdateAuditEntities();
            return base.SaveChanges();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            ConfiguraJogo(modelBuilder);
            ConfiguraPessoa(modelBuilder);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            UpdateAuditEntities();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync(cancellationToken);
        }


        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
        private void UpdateAuditEntities()
        {
            var modifiedEntries = ChangeTracker.Entries().Where(x => x.Entity is IBaseAuditableEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));


            foreach (var entry in modifiedEntries)
            {
                var entity = (IBaseAuditableEntity)entry.Entity;
                DateTime now = DateTime.UtcNow;

                if (entry.State == EntityState.Added)
                {
                    entity.CreatedDate = now;
                    entity.CreatedBy = CurrentUserId;
                }
                else
                {
                    base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                }

                entity.UpdatedDate = now;
                entity.UpdatedBy = CurrentUserId;
            }
        }






        private void ConfiguraJogo(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Jogo>()
                .ToTable("Jogos")
              .HasKey(x => new { x.Id });


            modelBuilder
              .Entity<Jogo>()
              .Property(x => x.Name)
              .HasColumnType("varchar(50)")
              .IsRequired();

            modelBuilder
            .Entity<Jogo>()
            .Property(x => x.Categoria)
            .HasColumnType("varchar(50)")
            .IsRequired();
        }

        private void ConfiguraPessoa(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Pessoa>()
                .ToTable("Pessoas")
              .HasKey(x => new { x.Id });


            modelBuilder
              .Entity<Pessoa>()
              .Property(x => x.Nome)
              .HasColumnType("varchar(100)")
              .IsRequired();
        }
    }
}
