﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebAPIJogo.Domain.Core.Interfaces.Repositorys.Base;
using WebAPIJogo.Infra.Data.EFCore.Context;

namespace WebAPIJogo.Infra.Data.Repository.Repositorys.Base
{
    public abstract class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        #region property 
        private readonly ApplicationContext _ctx;
        private readonly DbSet<TEntity> _dbSet;
        #endregion

        #region Constructor
        public RepositoryBase(ApplicationContext ctx)
        {
            _ctx = ctx;
            _dbSet = _ctx.Set<TEntity>();
        }
        #endregion

        #region methods
        public void Create(TEntity entity)
        {
            try
            {
                _dbSet.Add(entity);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Delete(TEntity entity)
        {
            try
            {
                this._dbSet.Remove(entity);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }

        public async Task<TEntity> FindByKeyAsync(object key)
        {

            var entity = await _dbSet.FindAsync(key);
            if (entity != null)
                _ctx.Entry(entity).State = EntityState.Detached;

            return entity;

        }

        public async Task<TEntity> FindByConditionAsync(Expression<Func<TEntity, bool>> expression, params Expression<Func<TEntity, object>>[] includes)
        {
            var entities = _dbSet.Where(expression).AsNoTracking();

            if (includes != null)
            {
                foreach (var prop in includes)
                {
                    entities = entities.Include(prop);
                }
            }
            return await entities.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await this._dbSet.AsNoTracking().ToListAsync();
        }

        public void Update(TEntity entity)
        {
            try
            {
                this._dbSet.Update(entity);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
    }
}
