﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPIJogo.Domain.Core.Interfaces.Repositorys;
using WebAPIJogo.Domain.Models;
using WebAPIJogo.Infra.Data.EFCore.Context;
using WebAPIJogo.Infra.Data.Repository.Repositorys.Base;

namespace WebAPIJogo.Infra.Data.Repository.Repositorys
{
    public class RepositoryEmprestimo : RepositoryBase<Emprestimo>, IRepositoryEmprestimo
    {
        private readonly ApplicationContext _ctx;

        #region Constructor
        public RepositoryEmprestimo(ApplicationContext ctx) : base(ctx)
        {


            _ctx = ctx;

        }
        #endregion

        #region methods opcionais    
        public new void Create(Emprestimo entity)
        {
            try
            {
                entity.Amigo = _ctx.Pessoas.Find(entity.PessoaId);
                entity.Jogo = _ctx.Jogos.Find(entity.JogoId);


                //criando emprestimo
                _ctx.Emprestimos.Add(entity);


                // atualizando o status do jogo disponivel para indisponivel
                entity.Jogo.Disponivel = false;
                _ctx.Jogos.Update(entity.Jogo);


                _ctx.SaveChanges();


                if (entity != null)
                    _ctx.Entry(entity).State = EntityState.Detached;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public new async Task<Emprestimo> FindByKeyAsync(object key)
        {

            var entity = await _ctx.Emprestimos
                .Include(i => i.Amigo)
                    .Include(j => j.Jogo)
                .AsNoTracking()
                .Where(a => a.Id == (Guid)key)
                .FirstOrDefaultAsync();

            if (entity != null)
            {
                //await _ctx.Emprestimos
                //    .Include(i => i.Amigo)
                //    .Include(j => j.Jogo)
                //    .LoadAsync();

                _ctx.Entry(entity).State = EntityState.Detached;
            }
            return entity;

        }
        public async Task<IEnumerable<Emprestimo>> GetByJogo(int JogoId)
        {

            var emprestimos = await _ctx.Emprestimos
                .Where(a => a.JogoId == JogoId)
                .AsNoTracking().ToListAsync();

            return emprestimos;
        }
        public async Task<IEnumerable<Emprestimo>> GetByPessoa(int PessoaId)
        {

            var emprestimos = await _ctx.Emprestimos
                .Where(a => a.PessoaId == PessoaId)
                .AsNoTracking().ToListAsync();

            return emprestimos;

        }
        public async Task<Emprestimo> GetByPessoaJogo(int PessoaId, int JogoId)
        {

            var emprestimos = await _ctx.Emprestimos
                .Where(a => a.PessoaId == PessoaId && a.JogoId == JogoId)
                .AsNoTracking().FirstOrDefaultAsync();

            return emprestimos;

        }
        public new async Task<IEnumerable<Emprestimo>> GetAllAsync()
        {
            return await _ctx.Emprestimos
                .Include(a => a.Amigo)
                .Include(j => j.Jogo)
                .ToListAsync();
        }
        public async Task<bool> FinalizarEmprestimo(Guid id)
        {
            var isFinalizado = false;
            using (var transation = await _ctx.Database.BeginTransactionAsync())
            {
                try
                {
                    var emprestimo = await _ctx.Emprestimos.FindAsync(id);


                    emprestimo.Entregue = true;
                    emprestimo.DataEntrega = DateTime.Now;

                    _ctx.Emprestimos.Update(emprestimo);
                    isFinalizado = await _ctx.SaveChangesAsync() >= 1;

                    var jogo = await _ctx.Jogos.FindAsync(emprestimo.JogoId);
                    jogo.Disponivel = true;
                    _ctx.Jogos.Update(emprestimo.Jogo);

                    isFinalizado = await _ctx.SaveChangesAsync() >= 1;
                    transation.Commit();


                }
                catch (Exception)
                {
                    transation.Rollback();
                }
            }

            return isFinalizado;

        }
        #endregion
    }
}
