﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using WebAPIJogo.Domain.Core.Interfaces.Repositorys;
using WebAPIJogo.Domain.Models;
using WebAPIJogo.Infra.Data.EFCore.Context;
using WebAPIJogo.Infra.Data.Repository.Repositorys.Base;

namespace WebAPIJogo.Infra.Data.Repository.Repositorys
{
    public class RepositoryJogo : RepositoryBase<Jogo>, IRepositoryJogo
    {
        #region Constructor
        public RepositoryJogo(ApplicationContext ctx) : base(ctx)
        {
        }
        #endregion
    }
}
