﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPIJogo.Domain.Core.Interfaces.Repositorys;
using WebAPIJogo.Domain.Models;
using WebAPIJogo.Infra.Data.EFCore.Context;
using WebAPIJogo.Infra.Data.Repository.Repositorys.Base;

namespace WebAPIJogo.Infra.Data.Repository.Repositorys
{
    public class RepositoryPessoa : RepositoryBase<Pessoa>, IRepositoryPessoa
    {
        private readonly ApplicationContext _ctx;

        #region Constructor
        public RepositoryPessoa(ApplicationContext ctx) : base(ctx)
        {
            _ctx = ctx;
        }
        #endregion

        #region methods   
        public new async Task<IEnumerable<Pessoa>> GetAllAsync()
        {
            return await this._ctx.Pessoas
                .AsNoTracking()
                .Include(i => i.Emprestimos)
                .ThenInclude(j => j.Jogo)
                .ToListAsync();
        }
        public async Task<Pessoa> GetEmprestimoPendente(int key)
        {
            var entity = await _ctx.Pessoas
                .Where(p => p.Id == key && p.Emprestimos.Any(a => a.Entregue == false))
                .Where(a => a.Id == key)
                .Include(i => i.Emprestimos)
                .ThenInclude(j => j.Jogo)
                .AsNoTracking()
                .FirstOrDefaultAsync();
            return entity;
        }
        #endregion
    }
}
