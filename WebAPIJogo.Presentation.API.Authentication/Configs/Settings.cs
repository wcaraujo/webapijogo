﻿namespace WebAPIJogo.Presentation.API.Authentication.Configs
{
    public static class Settings
    {
        public static string Secret = "fedaf7d8863b48e197b9287d492b708e";
    }

    public class AppSetting
    {
        public string UrlIsProducion { get; set; }
        public string UrlIsDevelopment { get; set; }
    }
}
