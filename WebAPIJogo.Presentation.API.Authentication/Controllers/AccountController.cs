﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using WebAPIJogo.Presentation.API.Authentication.Models;
using WebAPIJogo.Presentation.API.Authentication.Repositorys;
using WebAPIJogo.Presentation.API.Authentication.Services;

namespace WebAPIJogo.Presentation.API.Authentication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private const string USUARIO_SENHA_INVALIDO = "Ops, usuário ou senha inválidos!";

        [HttpPost("signIn")]
        [AllowAnonymous]
        public IActionResult SignIn([FromBody] User user)
        {
            try
            {
                if (user == null)
                    return BadRequest(USUARIO_SENHA_INVALIDO);

                // RECUPERA O USUARIO
                var isLogin = UserRepository.Get(user.Username, user.Password);

                // VERIFICA SE O USUARIO EXISTE
                if (isLogin == null)
                    return Unauthorized(USUARIO_SENHA_INVALIDO);

                // GERA TOKEN
                var Access_token = TokenService.GenerateToken(isLogin);


                if (Access_token == null)
                    throw new Exception("Falha ao gerar token. Por favor, tente novamente mais tarde.");




                return Ok(new { Access_token });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message.ToString());
            }
        }





        [HttpGet]
        [Route("Anonymous")]
        [AllowAnonymous]
        public string Anonymous() => String.Format($"Anonymous");


        [HttpGet]
        [Route("authenticated")]
        [Authorize]
        public string Authenticated() => String.Format("Autenticado - {0}", User.Identity.Name);

        [HttpGet]
        [Route("employee")]
        [Authorize(Roles = "employee")]
        public string Employee() => "Funcionário";

        [HttpGet]
        [Route("manager")]
        [Authorize(Roles = "manager")]
        public string Manager() => "Gerente";
    }
}
