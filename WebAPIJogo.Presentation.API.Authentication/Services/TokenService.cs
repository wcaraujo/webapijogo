﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WebAPIJogo.Presentation.API.Authentication.Configs;
using WebAPIJogo.Presentation.API.Authentication.Models;

namespace WebAPIJogo.Presentation.API.Authentication.Services
{
    public static class TokenService
    {

        /*
          Note que na linha quinze utilizamos a chave que criamos no Settings.cs para gerar um Chave Privada, 
        ou seja, só conseguirão desencriptar este Token com esta chave, que só existe no nosso servidor.         
         */
        public static string GenerateToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Settings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Username.ToString()),
                    new Claim(ClaimTypes.Role, user.Role.ToString())
                }),
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

    }
}
