﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPIJogo.Application.Interfaces;

namespace WebAPIJogo.Presentation.API.GerJogo.Controllers.Base
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BaseController : ControllerBase
    {
        protected readonly IApplicationServiceEmprestimo _applicationServiceEmprestimo;
        protected readonly IApplicationServiceJogo _applicationServiceJogo;
        protected readonly IApplicationServicePessoa _applicationServicePessoa;

        public BaseController(IApplicationServiceEmprestimo applicationServiceEmprestimo, IApplicationServiceJogo applicationServiceJogo, IApplicationServicePessoa applicationServicePessoa)
        {
            _applicationServiceEmprestimo = applicationServiceEmprestimo;
            _applicationServiceJogo = applicationServiceJogo;
            _applicationServicePessoa = applicationServicePessoa;
        }
    }
}
