﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Application.Interfaces;
using WebAPIJogo.Presentation.API.GerJogo.Controllers.Base;

namespace WebAPIJogo.Presentation.API.GerJogo.Controllers
{
    //a regra dessse emprestimo e o amigo so poder levar um jogo por vez e so locar outro quando tiver devolvlido o emprestimo
    public class EmprestimoController : BaseController
    {
        public EmprestimoController(IApplicationServiceEmprestimo applicationServiceEmprestimo, IApplicationServiceJogo applicationServiceJogo, IApplicationServicePessoa applicationServicePessoa) : base(applicationServiceEmprestimo, applicationServiceJogo, applicationServicePessoa)
        {

        }


        // GET: api/<JogoController>
        [HttpGet]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Get()
        {
            List<EmprestimoDTO> lista = (List<EmprestimoDTO>)await _applicationServiceEmprestimo.GetAllAsync();
            if (lista == null || lista.Count == 0)
                return NotFound("Ops, nenhum emprestimo cadastrado!");

            return Ok(lista);
        }

        // GET api/<JogoController>/5
        [HttpGet("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Get(Guid id)
        {
            var pessoa = await _applicationServiceEmprestimo.FindByKeyAsync(id);

            if (pessoa == null)
                return NotFound();


            return Ok(pessoa);
        }

        /// <summary>
        /// trazer todos os emprestimo do jogo em questão
        /// </summary>
        /// <param name="id"></param>
        /// <returns>emprestimo</returns>
        [HttpGet("GetByJogo/{idJogo}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> GetByJogo(int idJogo)
        {
            var emprestimos = await _applicationServiceEmprestimo.GetByJogo(idJogo);

            if (emprestimos == null)
                return NotFound();


            return Ok(emprestimos);
        }
        [HttpGet("GetByPessoa/{idPessoa}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> GetByPessoa(int idPessoa)
        {
            var emprestimos = await _applicationServiceEmprestimo.GetByPessoa(idPessoa);

            if (emprestimos == null)
                return NotFound();

            return Ok(emprestimos);
        }

        [HttpGet("GetByPessoaJogo/{idPessoa}/{idJogo}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> GetByPessoaJogo(int idPessoa, int idJogo)
        {
            var emprestimos = await _applicationServiceEmprestimo.GetByPessoaJogo(idPessoa, idJogo);

            if (emprestimos == null)
                return NotFound();

            return Ok(emprestimos);
        }


        // POST api/<JogoController>
        [HttpPost]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Post([FromBody] EmprestimoDTO value)
        {
            try
            {
                if (value == null)
                    return BadRequest();



                //verificando se emprestimo ja foi realizado
                var isEmprestimo = await _applicationServiceEmprestimo.GetByPessoaJogo((int)value.AmigoDTO.Id, (int)value.JogoDTO.Id);
                if (isEmprestimo != null && !isEmprestimo.Entregue)
                    return NotFound("Ops, emprestimo já realizado no momento. Por gentileza, tente novamente mais tarde!");



                //verificando se o jogo esta disponivel para emprestimo
                var jogo = await _applicationServiceJogo.FindByKeyAsync((int)value.JogoDTO.Id);
                if (!jogo.Disponivel)
                    return NotFound("Ops, jogo não está disponível no momento!");


                //verificando se o amigo tem algum emprestimo em aberto.
                var pessoa = await _applicationServicePessoa.FindByKeyAsync((int)value.AmigoDTO.Id);
                foreach (var item in pessoa.EmprestimosDTO)
                {
                    if (!item.Entregue)
                        return NotFound("Ops, voce possui um emprestimo pendente no momento não vai ser possível!");

                }



                value.DataEntrega = DateTime.Now.AddDays(3);

                //criando o emprestimo
                _applicationServiceEmprestimo.Create(value);


                return Ok("Emprestimo realizado com sucesso!");


            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }

        // PUT api/<JogoController>/5
        [HttpPut("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Put(Guid id, [FromBody] EmprestimoDTO value)
        {
            try
            {
                if (value == null)
                    return BadRequest();

                var emprestimo = await _applicationServiceEmprestimo.FindByKeyAsync(id);


                if (emprestimo == null)
                    return NotFound();

                emprestimo.DataDevolucao = value.DataDevolucao;
                emprestimo.DataEntrega = value.DataEntrega;
                emprestimo.Entregue = value.Entregue;

                _applicationServiceEmprestimo.Update(emprestimo);
                return Ok("Emprestimo atualizado com sucesso!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }



        // PUT api/<JogoController>/5
        [HttpPut("FinalizarEmprestimo/{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> FinalizarEmprestimo(Guid id)
        {
            try
            {

                var emprestimo = await _applicationServiceEmprestimo.FindByKeyAsync(id);
                if (emprestimo == null)
                    return NotFound();

                if (emprestimo.Entregue)
                    return BadRequest("Emprestimo já finalizado!");



                var isFinalizado = await _applicationServiceEmprestimo.FinalizarEmprestimo(emprestimo.Id);
                if (isFinalizado)
                    return Ok("Emprestimo finalizado com sucesso!");
                else
                    return BadRequest("Falha finalizar emprestimo!");



            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }
    }
}
