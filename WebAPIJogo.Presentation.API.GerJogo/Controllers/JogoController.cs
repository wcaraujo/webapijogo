﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Application.Interfaces;
using WebAPIJogo.Presentation.API.GerJogo.Controllers.Base;


namespace WebAPIJogo.Presentation.API.GerJogo.Controllers
{

    public class JogoController : BaseController
    {
        public JogoController(IApplicationServiceEmprestimo applicationServiceEmprestimo, IApplicationServiceJogo applicationServiceJogo, IApplicationServicePessoa applicationServicePessoa) : base(applicationServiceEmprestimo, applicationServiceJogo, applicationServicePessoa)
        {
        }



        // GET: api/<JogoController>
        [HttpGet]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Get()
        {
            List<JogoDTO> lista = (List<JogoDTO>)await _applicationServiceJogo.GetAllAsync();
            if (lista.Count == 0)
                return NotFound("Ops, nenhum registro cadastrado!");

            return Ok(lista);
        }

        // GET api/<JogoController>/5
        [HttpGet("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Get(int id)
        {
            var jogo = await _applicationServiceJogo.FindByKeyAsync(id);

            if (jogo == null)
                return NotFound();



            return Ok(jogo);
        }

        // POST api/<JogoController>
        [HttpPost]
        [Authorize(Roles = "employee,manager")]
        public IActionResult Post([FromBody] JogoDTO value)
        {
            try
            {
                if (value == null)
                    return BadRequest();


                _applicationServiceJogo.Create(value);
                return Ok("Jogo cadastrado com sucesso!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }

        // PUT api/<JogoController>/5
        [HttpPut("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Put(int id, [FromBody] JogoDTO value)
        {
            try
            {
                if (value == null)
                    return BadRequest();

                var jogo = await _applicationServiceJogo.FindByKeyAsync(id);

                if (jogo == null)
                    return NotFound();


                jogo.Ativo = value.Ativo;
                jogo.Categoria = value.Categoria ?? jogo.Categoria;
                jogo.Disponivel = value.Disponivel;
                jogo.Name = value.Name ?? jogo.Name;


                _applicationServiceJogo.Update(jogo);
                return Ok("Jogo atualizado com sucesso!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }

        [HttpPatch("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<ActionResult> Patch(int id, [FromBody] JsonPatchDocument<JogoDTO> patchJogoDTO)
        {
            try
            {
                if (patchJogoDTO == null)
                    return BadRequest();


                var jogoDTO = await _applicationServiceJogo.FindByKeyAsync(id);

                if (jogoDTO == null)
                    return NotFound();


                patchJogoDTO.ApplyTo(jogoDTO);

                _applicationServiceJogo.Update(jogoDTO);
                return Ok("Jogo atualizado com sucesso!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }



        // DELETE api/<JogoController>/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "manager")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var jogo = await _applicationServiceJogo.FindByKeyAsync(id);


                if (jogo == null)
                    return NotFound();


                _applicationServiceJogo.Delete(jogo);

                return Ok("Jogo removido com sucesso!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }
    }
}
