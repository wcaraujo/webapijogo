﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPIJogo.Application.DTO.DTO;
using WebAPIJogo.Application.Interfaces;
using WebAPIJogo.Presentation.API.GerJogo.Controllers.Base;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPIJogo.Presentation.API.GerJogo.Controllers
{

    public class PessoaController : BaseController
    {
        public PessoaController(IApplicationServiceEmprestimo applicationServiceEmprestimo, IApplicationServiceJogo applicationServiceJogo, IApplicationServicePessoa applicationServicePessoa) : base(applicationServiceEmprestimo, applicationServiceJogo, applicationServicePessoa)
        {
        }


        // GET: api/<JogoController>
        [HttpGet]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Get()
        {
            List<PessoaDTO> lista = (List<PessoaDTO>)await _applicationServicePessoa.GetAllAsync();
            if (lista.Count == 0)
                return NotFound("Ops, nenhum pessoa cadastrada!");

            return Ok(lista);
        }

        // GET api/<JogoController>/5
        [HttpGet("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Get(int id)
        {
            var pessoa = await _applicationServicePessoa.FindByKeyAsync(id);

            if (pessoa == null)
                return NotFound("Ops, pessoa não cadastrada!");


            return Ok(pessoa);
        }


        // GET api/<JogoController>/5
        [HttpGet("GetEmprestimoPendente/{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> GetEmprestimoPendente(int id)
        {
            var pessoa = await _applicationServicePessoa.GetEmprestimoPendente(id);

            if (pessoa == null)
                return NotFound("Ops, pessoa não cadastrada!");


            if (pessoa.EmprestimosDTO.Count == 0)
                return NotFound("nenhum emprestimo pendente");


            return Ok(new { emprestimoPendente = pessoa });
        }


        // POST api/<JogoController>
        [HttpPost]
        [Authorize(Roles = "employee,manager")]
        public IActionResult Post([FromBody] PessoaDTO value)
        {
            try
            {
                if (value == null)
                    return BadRequest();

                _applicationServicePessoa.Create(value);
                return Ok("Amigo cadastrado com sucesso!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }

        // PUT api/<JogoController>/5
        [HttpPut("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Put(int id, [FromBody] PessoaDTO value)
        {
            try
            {
                if (value == null)
                    return BadRequest();

                var pessoa = await _applicationServicePessoa.FindByKeyAsync(id);

                if (pessoa == null)
                    return NotFound();

                pessoa.Nome = value.Nome;

                _applicationServicePessoa.Update(pessoa);
                return Ok("Amigo atualizado com sucesso!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }

        // DELETE api/<JogoController>/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "manager")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var pessoa = await _applicationServicePessoa.FindByKeyAsync(id);


                if (pessoa == null)
                    return NotFound();


                _applicationServicePessoa.Delete(pessoa);

                return Ok("Amigo removido com sucesso!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }
    }
}
