using System;
using System.IO;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using WebAPIJogo.Presentation.API.GerJogo.Configs;

namespace WebAPIJogo.Presentation.API.GerJogo
{
    public class Program
    {

        public static int Main(string[] args)
        {

            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: false).Build();
            var appSetting = new AppSetting();
            config.GetSection("AppSetting").Bind(appSetting);


#if DEBUG
            var url = appSetting.UrlIsDevelopment;
#else
        var  url = appSetting.UrlIsProducion;
#endif




            Log.Logger = new LoggerConfiguration()
             .MinimumLevel.Debug()
             .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
             .Enrich.FromLogContext()
             .WriteTo.Console()
             .WriteTo.Seq($"{url}/")
             .CreateLogger();
            try
            {
                Log.Information("INICIANDO_API");
                CreateHostBuilder(args)
                     .Build()
                     .Run();
                Log.Information("API INICIDADA");
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "API FINALIZADA");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>()
                                        .UseIISIntegration()
                     .ConfigureLogging((hostingContext, logging) =>
                     {
                         logging.ClearProviders();
                         logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                         logging.AddConsole();
                         logging.AddDebug();
                         logging.AddEventSourceLogger();
                         logging.AddFile(hostingContext.Configuration.GetSection("Logging"));
                     });
                });
    }
}
