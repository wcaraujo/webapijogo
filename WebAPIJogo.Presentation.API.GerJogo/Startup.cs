using System.Text;
using Autofac;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using WebAPIJogo.Infra.CrossCutting.IOC.Modules;
using WebAPIJogo.Infra.Data.EFCore.Context;
using WebAPIJogo.Presentation.API.GerJogo.Configs;

namespace WebAPIJogo.Presentation.API.GerJogo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            var connection = Configuration["ConnectionStrings:SqlConnection"];
            //ATEN��O PARA RODAR CAMADA DE TESTE TEM QUE DESCOMENTAR IF DA ConnectionStrings
            //if (connection == null)
            //{
            //    connection = "Server=(localdb)\\mssqllocaldb;Database=dbJogo;Trusted_Connection=True;MultipleActiveResultSets=true";
            //}
            services.AddDbContext<ApplicationContext>(x => x.UseSqlServer(connection));
            services.AddSwaggerGen(s => s.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "API GERENCIADOR DE JOGOS",
                Version = "v1"
            }
            ));
            services.AddMemoryCache();

            var key = Encoding.ASCII.GetBytes(Settings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = true;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }


        public void ConfigureContainer(ContainerBuilder build)
        {
            build.RegisterModule(new ModuleIOC());
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "API GERENCIADOR DE JOGOS"));
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
