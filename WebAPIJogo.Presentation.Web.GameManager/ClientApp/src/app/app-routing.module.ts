import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { IndexComponent } from './components/index/index.component';
import { Error404Component } from "./components/error-pages/error404/error404.component";
import { BrowserModule } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";
import { AuthGuard } from './services/auth-guard.service';



const routes: Routes = [
  { path: "", redirectTo: "index", pathMatch: "full" },
  { path: 'login', component: LoginComponent },
  {
    path: 'index',
    component: IndexComponent,
    canActivate: [AuthGuard]
  },
  { path: '**', component: Error404Component, data: { title: 'Page Not Found' } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), BrowserModule, CommonModule],
  exports: [RouterModule],
  providers: [
    AuthGuard
  ]
})
export class AppRoutingModule { }
