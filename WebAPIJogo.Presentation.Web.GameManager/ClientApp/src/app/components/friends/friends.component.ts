import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ReactiveFormsModule, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { FriendsService } from 'src/app/services/friends.service';
import { LoansService } from 'src/app/services/loans.service';

@Component({
  selector: 'Friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})

export class FriendsComponent implements OnInit{

  friend_id;
  friendsList = [];
  friendForm: FormGroup;
  loading = false;
  submitted = false;
  selected;
  objSelected;
  add = false;
  trash = false;
  edit = false;
 error = false;


  subscription: Subscription;

  constructor(
    public friendsService: FriendsService,
    private formBuilder: FormBuilder,
    public loansService: LoansService,

  ){

   loansService.friend$.subscribe(friend => {
      this.friend_id = friend;
    });

  }

  setFriend(friend) {

    this.loansService.setFriend(friend);


  }
  select(friend){
    this.selected = friend.id;
    this.objSelected = friend;
    this.setFriend(friend);
    this.error = false;
  }

  closeSelected(){
    this.selected = 0;
    this.objSelected = 0;
    this.error = false;

    this.setFriend(0);
  }
  ngOnInit():void {
    this.friendForm = this.formBuilder.group({
      nome: ['', Validators.required]
    });
    this.onGetFriends();

  }
  get f() {
    return this.friendForm.controls;
  }

  onInsert(){
    this.submitted = true;

    if (this.friendForm.invalid) {
        return;
    }

    this.loading = true;
    this.friendsService.register(this.friendForm.value)
        .pipe(first())
        .subscribe(
            data => {
                //this.alertService.success('Registration successful', true);
                //this.router.navigate(['/login']);
                this.onGetFriends();
                this.add = false;
            },
            error => {
                //this.alertService.error(error);
                this.loading = false;
                if(error.status == 200){
                  this.onGetFriends();
                  this.add = false;
                }


            });
  }
  onDelete(id: number) {

    this.friendsService.delete(id).pipe(first()).subscribe(
      data =>{
        this.onGetFriends();
        this.add = false;
      },
      error => {
        if(error.status == 200){
          this.onGetFriends();
          this.add = false;
        }
        if(error.status == 403){

          this.error = true;
        }
      }
    );
  }
  onEdit(){
    this.submitted = true;

    if (this.friendForm.invalid) {
        return;
    }

    this.loading = true;
    console.log(this.selected)
    this.friendsService.edit(this.friendForm.value,this.selected)
        .pipe(first())
        .subscribe(
            data => {
                //this.alertService.success('Registration successful', true);
                //this.router.navigate(['/login']);
                this.onGetFriends();
                this.edit = false;
                this.selected = null;
                this.objSelected = null;
            },
            error => {
                //this.alertService.error(error);
                this.loading = false;
                console.log('Error')
                if(error.status == 200){
                  this.onGetFriends();
                  this.edit = false;
                  this.selected = null;
                  this.objSelected = null;
                }

            });
  }
  private onGetFriends(){
    this.friendsService.getAll().pipe(first()).subscribe(friends => this.friendsList = friends);
  }

}
