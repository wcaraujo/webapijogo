
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ReactiveFormsModule, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { GamesService } from 'src/app/services/games.service';
import { LoansService } from 'src/app/services/loans.service';
import { NotificationService } from '../../services/notification.service';

@Component({
  selector: 'Games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  friend;
  game_id = 0;

  gamesList = [];
  loansList;
  error;
  gamesForm: FormGroup;


  loading = false;
  submitted = false;
  add = false;
  edit = false;
  loanS = false;

  selected = 0;
  objSelected;


  subscription: Subscription;


  constructor(
    public gamesService: GamesService,
    public loansService: LoansService,
    private formBuilder: FormBuilder,
    private notification: NotificationService
  ) {


  }

  select(game) {

    this.objSelected = game;
    this.selected = game.id;
    this.setGame(game);
    this.error = false;

  }
  setGame(game) {
    this.loansService.setGame_id(game);
  }
  closeSelected() {

    this.selected = 0;
    this.setGame(0);
    this.error = false;
  }

  ngOnInit(): void {
    this.gamesForm = this.formBuilder.group({
      name: ['', Validators.required],
      categoria: 'teste',
      ativo: true,
      disponivel: true
    });
    this.onGetGames();
    this.getLoans();
  }
  get f() {
    return this.gamesForm.controls;
  }
  onInsert() {
    this.submitted = true;

    if (this.gamesForm.invalid) {
      return;
    }

    this.loading = true;

    this.gamesService.register(this.gamesForm.value)
      .pipe(first())
      .subscribe(
        data => {
          //this.alertService.success('Registration successful', true);
          //this.router.navigate(['/login']);
          this.onGetGames();
          this.add = false;
        },
        error => {
          //this.alertService.error(error);
          this.loading = false;
          console.log('Error')
          if (error.status == 200) {
            this.onGetGames();
            this.add = false;
          }
        });
  }

  onDelete(id: number) {
    this.gamesService.delete(id).pipe(first()).subscribe(
      data => { },
      error => {
        if (error.status == 200) {
          this.onGetGames();
        }
        if (error.status == 403) {
          this.error = true;

        }
      }
    );

    this.selected = 0;
  }

  onEdit() {
    this.submitted = true;

    if (this.gamesForm.invalid) {
      return;
    }

    this.loading = true;

    this.gamesService.edit(this.gamesForm.value, this.selected)
      .pipe(first())
      .subscribe(
        data => {
          //this.alertService.success('Registration successful', true);
          //this.router.navigate(['/login']);
          this.onGetGames();
          this.edit = false;
          this.selected = null;
          this.objSelected = null;
        },
        error => {
          //this.alertService.error(error);
          this.loading = false;
          console.log('Error')
          if (error.status == 200) {
            this.onGetGames();
            this.edit = false;
            this.selected = null;
            this.objSelected = null;
          }
        });
  }

  onGetGames() {
    this.gamesService.getAll().pipe(first()).subscribe(games => this.gamesList = games);
  }

  /*Emprestimo */


  getLoans() {
    //this.loansService.get(this.friend_id).pipe(first()).subscribe(loans => this.loansList = loans);
  }
  delLoan(id) {

    this.loansService.delete(id)
      .pipe(first())
      .subscribe(() => this.getLoans());

    this.selected = null;

    this.loanS = null;
  }
}
