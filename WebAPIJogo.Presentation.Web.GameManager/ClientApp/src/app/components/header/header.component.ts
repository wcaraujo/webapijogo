import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { JwtHelper } from 'src/app/services/jwt.service';

@Component({
  selector: 'Header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title = 'Header';
  profile = false;
  user;
  userInfo;
  constructor (
    public authenticationService: AuthenticationService,
    private router: Router,
    public jwt: JwtHelper

  ){
    this.user = this.authenticationService.currentUserValue;
    
    if (this.user) {
      this.profile = true;

      this.userInfo = jwt.decodeToken(this.user.access_token)
     console.log(this.userInfo)


    }

  }

  ngOnInit(){}


  onLogout(){
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    this.profile = false;
  }
}
