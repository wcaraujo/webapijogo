import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { AuthenticationService } from 'src/app/services/authentication.service';
import { LoansService } from 'src/app/services/loans.service';
import { NotificationService } from '../../services/notification.service';



@Component({
  selector: 'Index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent {

  title = 'gameManager';
  friend;
  game;
  msg: string;
  user = false;
  obj;
  constructor(private notifyService: NotificationService, private router: Router, public authenticationService: AuthenticationService, private notification: NotificationService) { }
}
