
import { Component, OnInit, Input } from '@angular/core';
import { formatDate } from '@angular/common';
import { FormBuilder, } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { LoansService } from 'src/app/services/loans.service';
import { ResourceLoader } from '@angular/compiler';
import { NotificationService } from '../../services/notification.service';
@Component({
  selector: 'Loans',
  templateUrl: './loans.component.html',
  styleUrls: ['./loans.component.css']
})
export class LoansComponent implements OnInit {

  @Input() method: string;

  friend_obj;
  game_obj;
  loan_obj;
  teste;
  loansList = [];

  mode = false;
  selected;
  objSelected;
  error = false;
  get_friend: Subscription;
  get_game: Subscription;
  get_loan: Subscription;



  constructor(
    public loansService: LoansService,
    private formBuilder: FormBuilder,
    private notification: NotificationService
  ) {

    this.get_friend = loansService.friend$.subscribe(
      friend => {
        this.friend_obj = friend;
      });
    this.get_game = loansService.game$.subscribe(
      game => {
        this.game_obj = game;
        this.error = false;
      });

  }

  select(loan) {

    this.objSelected = loan;
    this.selected = loan.id;
    this.error = false;

  }
  closeSelected() {

    this.selected = 0;
    this.objSelected = 0;
    this.error = false;

  }
  alterMode() {
    if (this.mode) {
      this.mode = false
    } else {
      this.mode = true;
    }
  }
  date(date) {
    var result
    result = formatDate(date, 'yyyy-MM-dd', 'en-US');
    return result;
  }


  ngOnInit(): void {

    this.onGetLoans();

  }

  onInsert() {
    var dt = new Date()
    const day = () => {
      if (dt.getDate() < 10) {
        return '0' + dt.getDate()
      } else {
        return dt.getDate()
      }
    }
    var date = dt.getFullYear() + "-" + dt.getMonth() + "-" + day();
    var loan = {
      amigoDTO: {
        id: this.friend_obj.id,
        nome: this.friend_obj.nome,
        emprestimosDTO: [null]
      },
      jogoDTO: {
        id: this.game_obj.id,
        name: this.game_obj.name,
        categoria: this.game_obj.categoria,
        ativo: true,
        disponivel: false
      },
      dataEmprestimo: date,
      dataDevolucao: date


    }
    this.loansService.register(loan)
      .subscribe(
        data => {
          alert(data);
        },
        error => {

          if (error.status != 200) {
            this.error = true;
            this.notification.showIsError();
          } else {
            this.notification.showIsSucesso();
            location.reload();
          }
        }
      );
  }

  onReturn() {

    this.loansService.back(this.selected).pipe(first()).subscribe(
      data => {
        console.log('success');
      },
      error => {
        if (error.status == 200) {
          this.onGetLoans();
        }
      }
    );

  }
  onGetLoans() {
    this.loansService.getAll().pipe(first()).subscribe(loans => this.loansList = loans);

  }
}
