import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReactiveFormsModule, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { NotificationService } from 'src/app/services/notification.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'Login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = false;

  constructor(private authenticationService: AuthenticationService, private formBuilder: FormBuilder, private router: Router, private notifyService: NotificationService) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/index']);
    }
    this.notifyService.showHTMLMessage('ADMIN USER<br>USERNAME:batman PASSWORD: batman <br> STANDARD USER <br>USERNAME:robin PASSWORD: robin', 'Login');
  }





  get f() {
    return this.loginForm.controls;
  }
  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      usuario: ['', Validators.required],
      senha: ['', Validators.required]
    });
  }
  onSubmit() {

    this.submitted = true;

    if (this.loginForm.invalid) {
      this.notifyService.showInfo('Ops, é necessario informar username/senha ', 'Login');
      return;

    }

    this.loading = true;

    this.authenticationService.login(this.f.usuario.value, this.f.senha.value)
      .pipe(first())
      .subscribe(
        data => {

          this.router.navigate(['/index']);

        },
        error => {
          this.notifyService.showWarning('Ops, username/senha invalidos', 'Login');
          this.error = true;
          this.loading = false;
        });

  }
  onLogout() {
    // this.authenticationService.logout();
  }
}
