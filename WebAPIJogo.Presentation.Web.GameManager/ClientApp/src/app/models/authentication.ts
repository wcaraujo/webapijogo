export class Authentication {
    id: number;
    usuario: string;
    senha: string;
    token: string;
}