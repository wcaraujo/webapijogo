export class Game {
    id: number;
    name: string;
    categoria: string;
    status: boolean;
    disponivel: boolean;
}
