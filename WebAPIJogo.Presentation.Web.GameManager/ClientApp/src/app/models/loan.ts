export class Loan {
    id: number;
    friend_id: number;
    friend: string;
    game_id: number;
    game: string;
    date_loan: Date;
    date_return: Date;
    delivered:boolean;

}
