
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Authentication } from './../models/authentication';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    apiUrl = 'https://gamemanager.auth.hcodetecnologia.com.br/';
    private currentUserSubject: BehaviorSubject<Authentication>;
    public currentUser: Observable<Authentication>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<Authentication>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): Authentication {
        return this.currentUserSubject.value;
    }

    login(username, password) {
        return this.http.post<any>(this.apiUrl+'api/Account/signIn', {id:0,username:username, password:password,roles:"string"})
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}
