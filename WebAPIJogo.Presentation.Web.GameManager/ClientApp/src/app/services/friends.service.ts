
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

import { Friend } from './../models/friend';

@Injectable({ providedIn: 'root' })
export class FriendsService {
    //apiUrl = 'https://5f9f4b111e9a4e001650559a.mockapi.io/';
    apiUrl = 'https://gamemanager.api.hcodetecnologia.com.br/';


    constructor(private http: HttpClient) { }

    getAll() {
      var hdr = this.headers();
        return this.http.get<Friend[]>(this.apiUrl+'api/Pessoa',{headers:hdr});
        //return this.http.get<Friend[]>(this.apiUrl+'friends');

    }

    register(friend: Friend) {
        var hdr = this.headers();
        return this.http.post(this.apiUrl+'api/Pessoa', friend,{headers:hdr});
    }

    delete(id: number) {
      var hdr = this.headers();
      return this.http.delete(this.apiUrl+'api/Pessoa/'+id,{headers:hdr});

        //return this.http.delete(this.apiUrl+'/friends/'+id);
    }
    edit(friend: Friend,id:number){
      var hdr = this.headers();
      return this.http.put(this.apiUrl+'api/Pessoa/'+id, friend,{headers:hdr});

    }
    headers(){
      var token = JSON.parse(localStorage.getItem('currentUser'));

      var reqHeader = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token.access_token
      });
      return reqHeader;
    }
}
