
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Game } from './../models/game';

@Injectable({ providedIn: 'root' })
export class GamesService {
    //apiUrl = 'https://5f9f4b111e9a4e001650559a.mockapi.io/';
    apiUrl = 'https://gamemanager.api.hcodetecnologia.com.br/';
    constructor(private http: HttpClient) { }

    getAll() {
        var hdr = this.headers();
        return this.http.get<Game[]>(this.apiUrl+'api/Jogo',{headers:hdr});
    }

    register(game : Game) {
        var hdr = this.headers();
        return this.http.post(this.apiUrl+'api/Jogo', game, {headers:hdr});
    }

    delete(id : number) {
        var hdr = this.headers();
        return this.http.delete(this.apiUrl+'api/Jogo/'+id,{headers:hdr});
    }
    edit(game : Game , id : number){
      var hdr = this.headers();
      return this.http.put(this.apiUrl+'api/Jogo/'+id, game,{headers:hdr});
    }
    headers(){
      var token = JSON.parse(localStorage.getItem('currentUser'));

      var reqHeader = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token.access_token
      });
      return reqHeader;
    }
}
