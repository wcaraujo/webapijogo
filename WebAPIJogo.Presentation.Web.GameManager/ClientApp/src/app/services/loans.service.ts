
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';

import { Loan } from './../models/loan';

@Injectable({ providedIn: 'root' })
export class LoansService {
    loansList = {};

    //apiUrl = 'https://5f9f4b111e9a4e001650559a.mockapi.io/';
    apiUrl = 'https://gamemanager.api.hcodetecnologia.com.br/';

    constructor(private http: HttpClient) { }
  /**/
    private pendenteSource = new Subject<number>();
    private concluidoSource = new Subject<object>();

    private game_obj = new Subject<Object>();
    private friend_obj = new Subject<Object>();


    // Observable string streams
    pendente$ = this.pendenteSource.asObservable();
    concluido$ = this.concluidoSource.asObservable();

    game$ = this.game_obj.asObservable();
    friend$ = this.friend_obj.asObservable();


    // Service message commands
    setGame_id(game: object){
      this.game_obj.next(game);
    }
    setFriend(friend: object){
      this.friend_obj.next(friend);

    }

    criar(tarefa: number) {
      this.pendenteSource.next(tarefa);
    }

    serviceToFriends(){}
    serviceToGames(){}
  /***/

    get(id:number){
      var hdr = this.headers();
      return this.http.get(this.apiUrl+'api/Emprestimo'+id,{headers:hdr});
    }
    getAll() {
      var hdr = this.headers();
        return this.http.get<Loan[]>(this.apiUrl+'api/Emprestimo',{headers:hdr});
    }

    register(loan) {
      var hdr = this.headers();
        return this.http.post(this.apiUrl+'api/Emprestimo', loan,{headers:hdr});
    }

    delete(id: number) {
      var hdr = this.headers();
        return this.http.delete(this.apiUrl+'api/Emprestimo/'+id,{headers:hdr});
    }
    back(id: string) {
      var hdr = this.headers();
      console.log(hdr);
        return this.http.put(this.apiUrl+'api/Emprestimo/FinalizarEmprestimo/'+id,null,{headers:hdr});
    }
    headers(){
      var token = JSON.parse(localStorage.getItem('currentUser'));

      var reqHeader = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token.access_token
      });
      return reqHeader;
    }

}
