import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr: ToastrService) { }

  showHTMLMessage(message, title) {


    this.toastr.info(message, title, {
      enableHtml: true
    });






  }
  showSuccess(message, title) {
    this.toastr.success(message, title)
  }

  showError(message, title) {
    this.toastr.error(message, title)
  }

  showInfo(message, title) {
    this.toastr.info(message, title)
  }

  showWarning(message, title) {
    this.toastr.warning(message, title)
  }

  showIsSucesso() { this.showSuccess(this.msgFeed.success, this.msgFeedTitle.success); }
  showIsError() { this.showError(this.msgFeed.error, this.msgFeedTitle.error); }

  private iconClasses = {
    error: 'toast-error',
    info: 'toast-info',
    success: 'toast-success',
    warning: 'toast-warning'
  };
  public msgFeed = {
    error: 'Error na operação!',
    success: 'Operação efetuada com sucesso',
    noEmail: 'Ops, endereço de e-mail incorreto',
    noTelefone: 'Ops, número de telefone incorreto',
    campoIsRequired: '* Campo requerido',
    captchaIsRequired: '* Captcha é requerido',
    EnviadoSucesso: 'enviado com sucesso!',
    EnviarEmailFalha: 'falha ao enviar email! Por favor, tente novamente mais tarde.'
  };
  public msgFeedTitle = {
    error: 'Error!',
    success: 'Sucesso!',
    parabens: 'Parabens!',
    aviso: 'Aviso!',
    info: 'Informação',
    Email: 'Email'
  };
}
