﻿namespace WebAPIJogo.Presentation.Web.ManagerGame.Configs
{

    public class AppSetting
    {
        public string UrlIsProducion { get; set; }
        public string UrlIsDevelopment { get; set; }
        public string UrlGameManagerAuthentication { get; set; }
        public string UrlGameMangerWebAPI { get; set; }
    }
}
