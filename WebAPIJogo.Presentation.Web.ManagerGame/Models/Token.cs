﻿using System.Runtime.Serialization;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Models
{
    public class Token
    {
        [DataMember(Name = "access_token")]
        public string Access_token { get; set; }

    }
}
