﻿using System.ComponentModel.DataAnnotations;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Models
{
    public class User
    {     
        public string Username { get; set; }     
        public string Password { get; set; }       
    }
}
