﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Pages.Home.Amigo
{

    [Authorize(Roles = "employee,manager")]
    public class CreateModel : PageModel
    {
        private readonly IWrapperDataStore _wrapperDataStore;
        public CreateModel(IWrapperDataStore wrapperDataStore)
        {
            _wrapperDataStore = wrapperDataStore;
        }

        [BindProperty]
        public AmigoViewModel Amigo { get; set; }
        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPostCreateAmigo()
        {
            if (!ModelState.IsValid)
            {
                ModelState.Clear();
                ModelState.AddModelError("Aviso", "Ops, é necessario preencher todos os campos!");
                return Page();

            }
            var isCreated = await _wrapperDataStore.AmigoDataStoreService.Create(Amigo);

            if (!isCreated)
                TempData["warning"] = "Falha cadastrar registro!";
            else
                TempData["success"] = "Registro cadastrado com sucesso!";

            return RedirectToPage("../Index");
        }


    }
}