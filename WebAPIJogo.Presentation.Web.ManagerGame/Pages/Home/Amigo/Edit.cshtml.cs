﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Pages.Home.Amigo
{
    [Authorize(Roles = "employee,manager")]
    public class EditModel : PageModel
    {

        private readonly IWrapperDataStore _wrapperDataStore;

        public EditModel(IWrapperDataStore wrapperDataStore)
        {
            _wrapperDataStore = wrapperDataStore;
        }

        [BindProperty]
        public AmigoViewModel Amigo { get; set; }
        public async Task<IActionResult> OnGet(int? id)
        {
            if (id == null)
                return BadRequest();



            Amigo = await _wrapperDataStore.AmigoDataStoreService.FindByKeyAsync(id);
            if (Amigo == null)
                return NotFound();




            return Page();
        }


        public async Task<IActionResult> OnPost()
        {

            if (!ModelState.IsValid)
            {
                ModelState.Clear();
                ModelState.AddModelError("Aviso", "Ops, é necessario preencher todos os campos!");
                return Page();

            }
            var isCreated = await _wrapperDataStore.AmigoDataStoreService.Update(Amigo.Id, Amigo);

            if (!isCreated)
                TempData["warning"] = "Falha atualizar registro!";
            else
                TempData["success"] = "Registro atualizado com sucesso!";

            return RedirectToPage("../Index");
        }
    }
}