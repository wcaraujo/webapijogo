﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Pages.Home.Emprestimo
{
    public class CreateModel : PageModel
    {
        private readonly IWrapperDataStore _wrapperDataStore;


        [BindProperty]
        public EmprestimoViewModel Emprestimo { get; set; }
        public CreateModel(IWrapperDataStore wrapperDataStore)
        {
            _wrapperDataStore = wrapperDataStore;
        }

        public void OnGet()
        {

        }


        public async Task<IActionResult> OnPostCreateEmprestimo()
        {
            if (!ModelState.IsValid)
                return Page();

            var created = await _wrapperDataStore.EmprestimoDataStore.Create(Emprestimo);

            if (!created)
                TempData["warning"] = "Falha cadastrar registro!";
            else
                TempData["success"] = "Registro cadastrado com sucesso!";

            return RedirectToPage("./Index");
        }
    }
}