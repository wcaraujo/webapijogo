﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Pages.Home.Emprestimo
{

    [Authorize(Roles = "employee,manager")]
    public class LendModel : PageModel
    {
        private readonly IWrapperDataStore _wrapperDataStore;


        [BindProperty]
        public EmprestimoViewModel Emprestimo { get; set; }

        [BindProperty]
        public IEnumerable<AmigoViewModel> Amigos { get; set; }


        public LendModel(IWrapperDataStore wrapperDataStore)
        {
            _wrapperDataStore = wrapperDataStore;
        }

        public async Task<IActionResult> OnGet(int? id)
        {
            if (id == null)
                return RedirectToPage("../Index");

            var isEmprestado = await _wrapperDataStore.JogoDataStoreService.FindByKeyAsync(id);
            if (!isEmprestado.Ativo)
            {
                TempData["warning"] = $"Ops, esse jogo se encontra bloquado para emprestimos. Por favor, tente novamente mais tarde!";
                return RedirectToPage("../Index");
            }

            if (!isEmprestado.Disponivel)
            {
                TempData["warning"] = $"Ops, esse jogo se encontra emprestado. Por favor, tente novamente mais tarde!";
                return RedirectToPage("../Index");
            }




            Emprestimo = new EmprestimoViewModel
            {
                JogoDTO = await _wrapperDataStore.JogoDataStoreService.FindByKeyAsync(id)
            };
            TempData["idJogo"] = Emprestimo.JogoDTO.Id;


            if (Emprestimo.JogoDTO.Disponivel == false)
                return RedirectToPage("../Index");

            Emprestimo.DataEmprestimo = DateTime.Now;
            Emprestimo.DataDevolucao = DateTime.Now.AddDays(4);
            Emprestimo.DataEntrega = DateTime.Now;


            Amigos = await _wrapperDataStore.AmigoDataStoreService.GetAllAsync();



            if (Amigos == null)
                return RedirectToPage("../Index");

            if (Emprestimo.JogoDTO == null)
                return RedirectToPage("../Index");


            return Page();
        }


        public async Task<IActionResult> OnPostCreateEmprestimo()
        {

            int idJogo = (int)TempData["idJogo"];
            Emprestimo.JogoDTO = new JogoViewModel() { Id = idJogo };

            //verifico se a pessoa já tem emprestimo ativo
            bool noEntregue = false;
            var isEmprestimo = await _wrapperDataStore.EmprestimoDataStoreService.GetByPessoa((int)Emprestimo.AmigoDTO.Id);
            if (isEmprestimo != null && isEmprestimo.Count() > 0)
            {
                foreach (var item in isEmprestimo)
                {
                    if (!item.Entregue)
                    {
                        noEntregue = true;
                        break;
                    }
                }
                if (noEntregue)
                {
                    TempData["warning"] = $"Já existe um emprestimo nesse nome";
                    await OnGet(Emprestimo.JogoDTO.Id);
                    ModelState.Clear();
                    return Page();
                }

            }


            //registro o jogo
            var created = await _wrapperDataStore.EmprestimoDataStoreService.Create(Emprestimo);
            if (!created)
                TempData["warning"] = "Falha registrar emprestimo!";
            else
                TempData["success"] = "emprestimo registrdo com sucesso!";


            return RedirectToPage("../Index");
        }
    }
}