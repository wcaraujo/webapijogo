﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Pages.Admin
{
    [Authorize(Roles = "employee,manager")]
    public class IndexModel : PageModel
    {
        [BindProperty]
        public string PerfilAcesso { get; set; }
        private string Role { get; set; }
        public string CurrentFilter { get; set; }
        [BindProperty]
        public List<JogoViewModel> Jogos { get; set; } = new List<JogoViewModel>();
        [BindProperty]
        public List<AmigoViewModel> Amigos { get; set; } = new List<AmigoViewModel>();
        [BindProperty]
        public List<EmprestimoViewModel> Emprestimos { get; set; } = new List<EmprestimoViewModel>();


        private readonly IWrapperDataStore _wrapperDataStore;
        private ClaimsPrincipal _claimsPrincipal;
        public IndexModel(IWrapperDataStore wrapperDataStore)
        {
            _wrapperDataStore = wrapperDataStore;

        }

        public async Task OnGet()
        {

            Role = GetRole();
            PerfilAcesso = Role.Equals("employee") ? "Funcionário" : "Gerente";
            Jogos = await _wrapperDataStore.JogoDataStoreService.GetAllAsync();
            Amigos = await _wrapperDataStore.AmigoDataStoreService.GetAllAsync();
            Emprestimos = await _wrapperDataStore.EmprestimoDataStoreService.GetAllAsync();
        }



        public async Task<IActionResult> OnGetDeleteJogo(int? id)
        {
            if (id == null)
                return NotFound();

            Role = GetRole();
            if (!Role.Equals("manager"))
            {
                TempData["warning"] = "Ops, somente  Gerente esta autorizado a realizar exclusões!";
                return RedirectToPage("./Index");
            }

            var emprestados = await _wrapperDataStore.EmprestimoDataStoreService.GetByJogo((int)id);
            if (emprestados.Count() >= 1)
            {
                TempData["warning"] = "Ops, esse jogo nao pode ser excluido pois já existe historico!";
                return RedirectToPage("./Index");
            }


            var isDeleted = await _wrapperDataStore.JogoDataStoreService.Delete(id);
            if (!isDeleted)
                TempData["warning"] = "Falha excluir registro!";
            else
                TempData["success"] = "Registro excluído com sucesso!";

            //  return Page();
            return RedirectToPage("./Index");
        }
        public async Task<IActionResult> OnGetDeleteAmigo(int? id)
        {
            if (id == null)
                return NotFound();


            Role = GetRole();
            if (!Role.Equals("manager"))
            {
                TempData["warning"] = "Ops, somente  Gerente esta autorizado a realizar exclusões!";
                return RedirectToPage("./Index");
            }

            var emprestados = await _wrapperDataStore.EmprestimoDataStoreService.GetByPessoa((int)id);
            if (emprestados.Count() >= 1)
            {
                TempData["warning"] = "Ops, esse jogo nao pode ser excluido pois já existe historico!";
                return RedirectToPage("./Index");
            }
            var isDeleted = await _wrapperDataStore.AmigoDataStoreService.Delete(id);
            if (!isDeleted)
                TempData["warning"] = "Falha excluir registro!";
            else
                TempData["success"] = "Registro excluído com sucesso!";

            //  return Page();
            return RedirectToPage("./Index");
        }

        public async Task<IActionResult> OnGetCancelEmprestimo(Guid? id)
        {
            if (id == null)
                return NotFound();


            Role = GetRole();
            if (!Role.Equals("manager"))
            {
                TempData["warning"] = "Ops, somente  Gerente esta autorizado a cancelar emprestimos!";
                return RedirectToPage("./Index");
            }

            var emprestimo = await _wrapperDataStore.EmprestimoDataStoreService.FindByKeyAsync(id);
            if (emprestimo == null)
            {
                TempData["warning"] = "Desculpe, mas o emprestimo  foi não localizado!";
                return RedirectToPage("./Index");
            }

            var situacao = emprestimo.Entregue ? emprestimo.DataEntrega.Year < emprestimo.DataEmprestimo.Year ? "Cancelado" : "Entregue" : "Pendente";
            if (situacao.Equals("Cancelado") || situacao.Equals("Entregue"))
            {
                TempData["warning"] = $"Ops, emrprestimo já foi {situacao}!";
                return RedirectToPage("./Index");

            }


            //cancelado emprestimo 
            emprestimo.Entregue = true;
            DateTime canceld = emprestimo.DataEmprestimo.AddYears(-1);
            emprestimo.DataEntrega = canceld;


    
            var isUpdate = await _wrapperDataStore.EmprestimoDataStoreService.Update(id, emprestimo);
            if (isUpdate)
            {
                //retornado o status do emprestimo;
                emprestimo.JogoDTO.Disponivel = true;
                isUpdate = await _wrapperDataStore.JogoDataStoreService.Update(emprestimo.JogoDTO.Id, emprestimo.JogoDTO);
            }

            if (!isUpdate)
                TempData["warning"] = "Falha excluir registro!";
            else
                TempData["success"] = "Emprestimo cancelado com sucesso!";

            //  return Page();
            return RedirectToPage("./Index");
        }

        public async Task<IActionResult> OnGetReceiveEmprestimo(Guid? id)
        {
            if (id == null)
                return NotFound();


            var emprestimo = await _wrapperDataStore.EmprestimoDataStoreService.FindByKeyAsync(id);
            if (emprestimo == null)
            {
                TempData["warning"] = "Desculpe, mas o emprestimo  foi não localizado!";
                return Page();
            }


            var situacao = emprestimo.Entregue ? emprestimo.DataEntrega.Year < emprestimo.DataEmprestimo.Year ? "Cancelado" : "Entregue" : "Pendente";
            if (situacao.Equals("Cancelado") || situacao.Equals("Entregue"))
            {
                TempData["warning"] = $"Ops, emrprestimo já foi {situacao}!";
                return RedirectToPage("./Index");

            }


            var isfinalizado = await _wrapperDataStore.EmprestimoDataStoreService.FinalizarEmprestimo((Guid)id);           
            if (!isfinalizado)            
                TempData["error"] = "Falha finalizar emprestimo!";            
            else
                TempData["success"] = "Emprestimo finalizado com sucesso!";

            //  return Page();
            return RedirectToPage("./Index");
        }

        public async Task<IActionResult> OnPostLogOff()
        {
            try
            {
                // Sign Out.  
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            }
            catch (Exception ex)
            {
                // Info  
                throw ex;
            }

            // Info.  
            return this.RedirectToPage("/Index");
        }

        private string GetRole()
        {
            // var IsRole = _claimsPrincipal.IsInRole("employee");
            _claimsPrincipal = HttpContext.User;
            return _claimsPrincipal.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).FirstOrDefault();
        }

    }


}

//TempData["warning"] = "Mensagem de warning!!";
//            TempData["success"] = "Mensagem de sucesso!!";
//            TempData["info"] = "Mensagem de informação!!";
//            TempData["error"] = "Mensagem de erro!!";