﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Pages.Home.Jogo
{

    [Authorize(Roles = "employee,manager")]
    public class CreateModel : PageModel
    {
        private readonly IWrapperDataStore _wrapperDataStore;

        public CreateModel(IWrapperDataStore wrapperDataStore)
        {
            _wrapperDataStore = wrapperDataStore;
        }
        [BindProperty]
        public JogoViewModel Jogo { get; set; }
        public void OnGet()
        {

        }
        public async Task<IActionResult> OnPostCreateJogo()
        {
            if (!ModelState.IsValid)
            {
                ModelState.Clear();
                ModelState.AddModelError("Aviso","Ops, é necessario preencher todos os campos!");
                return Page();

            }


            Jogo.Disponivel = true;
            var isCreated = await _wrapperDataStore.JogoDataStoreService.Create(Jogo);

            if (!isCreated)
                TempData["warning"] = "Falha cadastrar registro!";
            else
                TempData["success"] = "Registro cadastrado com sucesso!";

            return RedirectToPage("../Index");
        }
    }
}