﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Pages.Home.Jogo
{
    [Authorize(Roles = "employee,manager")]
    public class EditModel : PageModel
    {
        private readonly IWrapperDataStore _wrapperDataStore;

        public EditModel(IWrapperDataStore wrapperDataStore)
        {
            _wrapperDataStore = wrapperDataStore;
        }



        [BindProperty]
        public JogoViewModel Jogo { get; set; }
        public async Task<IActionResult> OnGet(int? id)
        {
            if (id == null)
                return BadRequest();



            Jogo = await _wrapperDataStore.JogoDataStoreService.FindByKeyAsync(id);
            if (Jogo == null)
                return NotFound();




            return Page();
        }


        public async Task<IActionResult> OnPost()
        {

            if (!ModelState.IsValid)
            {
                ModelState.Clear();
                ModelState.AddModelError("Aviso", "Ops, é necessario preencher todos os campos!");
                return Page();

            }


            if (!Jogo.Disponivel)
                Jogo.Ativo = true;


            var isCreated = await _wrapperDataStore.JogoDataStoreService.Update(Jogo.Id, Jogo);

            if (!isCreated)
                TempData["warning"] = "Falha atualizar registro!";
            else
                TempData["success"] = "Registro atualizado com sucesso!";

            return RedirectToPage("../Index");
        }
    }
}