﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IWrapperDataStore _wrapperDataStore;
        public IndexModel(ILogger<IndexModel> logger, IWrapperDataStore wrapperDataStore)
        {
            _logger = logger;
            _wrapperDataStore = wrapperDataStore;
        }

        /// <summary>  
        /// Gets or sets login model property.  
        /// </summary>  
        [BindProperty]
        public LoginViewModel LoginModel { get; set; }
        public IActionResult OnGet()
        {
            // Verification.  
            if (this.User.Identity.IsAuthenticated)
                return this.RedirectToPage("/Home/Index");
            else
            {

                TempData["info"] = "ADMIN USER<br>USERNAME:batman PASSWORD: batman<br>STANDARD USER <br>USERNAME:robin PASSWORD: robin";

                return Page();
            }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            try
            {


                if (ModelState.IsValid)
                {


                    var token = await _wrapperDataStore.AccountDataStoreService.SignIn(LoginModel.Username, LoginModel.Password);
                    if (token == null)
                    {
                        ModelState.Clear();
                        ModelState.AddModelError(nameof(LoginViewModel.Username), "usuario inexistente");
                        ModelState.AddModelError(nameof(LoginViewModel.Password), "Senha inexistente");

                        return Page();
                    }


                    var handler = new JwtSecurityTokenHandler();
                    var credencias = handler.ReadJwtToken(token.Access_token) as JwtSecurityToken;
                    var role = credencias.Claims.Where(c => c.Type == "role").Select(c => c.Value).FirstOrDefault();

                    // Login In.  
                    await this.SignInUser(LoginModel.Username, role, token.Access_token, false);



                    return RedirectToPage("/Home/Index");
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
            return Page();

        }

        public IActionResult OnGetLogoutAsync()
        {
            return RedirectToPage("/Index");
        }

        #region Sign In method.  

        /// <summary>  
        /// Sign In User method.  
        /// </summary>  
        /// <param name="username">Username parameter.</param>  
        /// <param name="isPersistent">Is persistent parameter.</param>  
        /// <returns>Returns - await task</returns>  
        private async Task SignInUser(string username, string role, string token, bool isPersistent)
        {
            // Initialization.  
            var claims = new List<Claim>();

            try
            {
                // Setting  


                claims.Add(new Claim(ClaimTypes.Name, username));
                claims.Add(new Claim(ClaimTypes.Role, role));
                claims.Add(new Claim("token", token));


                var claimIdenties = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimPrincipal = new ClaimsPrincipal(claimIdenties);
                var authenticationManager = Request.HttpContext;

                // Sign In.  
                await authenticationManager.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimPrincipal, new AuthenticationProperties() { IsPersistent = isPersistent });
            }
            catch (Exception ex)
            {
                // Info  
                throw ex;
            }
        }

        #endregion
    }
}
