using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using WebAPIJogo.Presentation.Web.ManagerGame.Configs;

namespace WebAPIJogo.Presentation.Web.ManagerGame
{
    public class Program
    {
        public static int Main(string[] args)
        {

            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: false).Build();
            var appSetting = new AppSetting();
            config.GetSection("AppSetting").Bind(appSetting);


#if DEBUG
            var url = appSetting.UrlIsDevelopment;
#else
        var  url = appSetting.UrlIsProducion;
#endif


            Log.Logger = new LoggerConfiguration()
             .MinimumLevel.Debug()
             .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
             .Enrich.FromLogContext()
             .WriteTo.Console()
             .WriteTo.Seq($"{url}/")
             .CreateLogger();
            try
            {
                Log.Information("INICIANDO_APPLICACAO");
                CreateHostBuilder(args)
                     .Build()
                     .Run();
                Log.Information("APPLICACAO INICIDADA");
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "APLICACAO FINALIZADA");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>()
                                        .UseIISIntegration()
                                   .UseContentRoot(Directory.GetCurrentDirectory())
                     .ConfigureLogging((hostingContext, logging) =>
                     {
                         logging.ClearProviders();
                         logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                         logging.AddConsole();
                         logging.AddDebug();
                         logging.AddEventSourceLogger();
                         logging.AddFile(hostingContext.Configuration.GetSection("Logging"));
                     });
                });
    }
}
