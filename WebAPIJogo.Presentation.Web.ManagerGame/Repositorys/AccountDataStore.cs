﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Configs;
using WebAPIJogo.Presentation.Web.ManagerGame.Models;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Base;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Repositorys
{
    public class AccountDataStore : BaseDataStore<User>, IAccountDataStore
    {
        public AccountDataStore(IOptions<AppSetting> options, IHttpContextAccessor httpContextAccessor) : base(options, httpContextAccessor)
        {
        }

        public async Task<Token> SignIn(string username, string password)
        {
            if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(password))
                throw new Exception($"PARAMETRO_ENTRADA_NULO");

            User userAuthLoginViewModel = new User() { Username = username, Password = password };
            var serializedItem = JsonConvert.SerializeObject(userAuthLoginViewModel);
            var response = await _httpClient.PostAsync(_endPointAccount, new StringContent(serializedItem, Encoding.UTF8, "application/json"));
            var result = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
                return null;
            else
                return JsonConvert.DeserializeObject<Token>(result);
        }
    }
}
