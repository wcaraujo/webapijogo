﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Configs;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Base;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Repositorys
{
    public class AmigoDataStore : BaseDataStore<AmigoViewModel>, IAmigoDataStore
    {
        public AmigoDataStore(IOptions<AppSetting> options, IHttpContextAccessor httpContextAccessor) : base(options, httpContextAccessor)
        {
        }

        #region methods opcionais
        public async Task<AmigoViewModel> GetEmprestimoPendente(int key)
        {

            var response = await _httpClient.GetAsync(_endPointPessoa);
            var result = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<AmigoViewModel>(result);
        }
        #endregion
    }
}
