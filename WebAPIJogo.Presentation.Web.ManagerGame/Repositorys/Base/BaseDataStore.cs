﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Configs;
using WebAPIJogo.Presentation.Web.ManagerGame.Models;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts.Base;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Base
{
    public class BaseDataStore<TEntity> : IDisposable, IBaseDataStore<TEntity> where TEntity : class
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ClaimsPrincipal _claimsPrincipal;
        protected readonly HttpClient _httpClient;
        protected readonly string _endPointAccount = "api/account/signIn";
        protected readonly string _endPointJogo = "api/jogo";
        protected readonly string _endPointPessoa = "api/pessoa";
        protected readonly string _endPointEmprestimo = "api/emprestimo";
        private string _endPoint;

        public readonly AppSetting _appSetting;
        public BaseDataStore(IOptions<AppSetting> options, IHttpContextAccessor httpContextAccessor)
        {
            if (options.Value == null)
                throw new Exception($"Ops, é necessario informar as url de acesso das aplicações (UrlGameManagerAuthenticatio e UrlGameMangerWebAPI) ");



            _appSetting = options.Value;

            Type typeParameterType = typeof(TEntity);
            string baseAddress = _appSetting.UrlGameManagerAuthentication; //  _uriStringAccount;
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Clear();


            if (typeParameterType.Name != typeof(User).Name)
            {
                baseAddress = _appSetting.UrlGameMangerWebAPI; //_uriStringGameManager;
                _httpContextAccessor = httpContextAccessor;

                if (_httpContextAccessor.HttpContext != null)
                    _claimsPrincipal = httpContextAccessor.HttpContext.User;

                var accessToken = _claimsPrincipal.Claims.Where(c => c.Type == "token").Select(c => c.Value).FirstOrDefault();
                _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            }


            _httpClient.BaseAddress = new Uri(baseAddress);


            if (typeParameterType.Name == typeof(JogoViewModel).Name)
                _endPoint = _endPointJogo;
            else if (typeParameterType.Name == typeof(AmigoViewModel).Name)
                _endPoint = _endPointPessoa;
            else if (typeParameterType.Name == typeof(EmprestimoViewModel).Name)
                _endPoint = _endPointEmprestimo;

        }



        public async Task<bool> Create(TEntity obj)
        {
            if (obj == null)
                throw new Exception($"PARAMETRO_ENTRADA_NULO");

            var serializedItem = JsonConvert.SerializeObject(obj);
            var response = await _httpClient.PostAsync(_endPoint, new StringContent(serializedItem, Encoding.UTF8, "application/json"));
            await response.Content.ReadAsStringAsync();
            return response.IsSuccessStatusCode;
        }

        public async Task<bool> Delete(object key)
        {
            if (key == null)
                throw new Exception($"PARAMETRO_ENTRADA_NULO");

            var response = await _httpClient.DeleteAsync($"{_endPoint}/{key}");
            await response.Content.ReadAsStringAsync();
            return response.IsSuccessStatusCode;
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }

        public async Task<TEntity> FindByKeyAsync(object key)
        {
            if (key == null)
                throw new Exception($"PARAMETRO_ENTRADA_NULO");

            var response = await _httpClient.GetAsync($"{_endPoint}/{key}");
            var result = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<TEntity>(result);
        }

        public async Task<List<TEntity>> GetAllAsync()
        {
            var response = await _httpClient.GetAsync(_endPoint);
            var result = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<List<TEntity>>(result);
        }

        public async Task<bool> Update(object key, TEntity obj)
        {
            if (key == null || obj == null)
                throw new Exception($"PARAMETRO_ENTRADA_NULO");

            var serializedItem = JsonConvert.SerializeObject(obj);
            var response = await _httpClient.PutAsync($"{_endPoint}/{key}", new StringContent(serializedItem, Encoding.UTF8, "application/json"));
            await response.Content.ReadAsStringAsync();
            return response.IsSuccessStatusCode;
        }
    }
}
