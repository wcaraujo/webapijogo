﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts.Base
{
    public interface IBaseDataStore<TEntity> where TEntity : class
    {
        #region methods
        Task<bool> Create(TEntity obj);
        Task<TEntity> FindByKeyAsync(object key);
        Task<List<TEntity>> GetAllAsync();
        Task<bool> Update(object key,TEntity obj);
        Task<bool> Delete(object key);
        #endregion
    }
}
