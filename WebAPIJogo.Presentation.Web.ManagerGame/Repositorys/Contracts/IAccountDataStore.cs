﻿using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Models;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts
{
    public interface IAccountDataStore
    {
        Task<Token> SignIn(string username, string password);
    }
}
