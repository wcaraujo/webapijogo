﻿using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts.Base;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts
{
    public interface IAmigoDataStore : IBaseDataStore<AmigoViewModel>
    {
        #region methods opcionais
        Task<AmigoViewModel> GetEmprestimoPendente(int key);
        #endregion
    }
}
