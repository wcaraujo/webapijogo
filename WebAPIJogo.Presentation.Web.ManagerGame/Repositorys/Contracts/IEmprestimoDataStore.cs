﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts.Base;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts
{
    public interface IEmprestimoDataStore : IBaseDataStore<EmprestimoViewModel>
    {


        #region methods opcionais
        Task<List<EmprestimoViewModel>> GetByJogo(int JogoId);
        Task<IEnumerable<EmprestimoViewModel>> GetByPessoa(int PessoaId);
        Task<EmprestimoViewModel> GetByPessoaJogo(int PessoaId, int JogoId);
        Task<bool> FinalizarEmprestimo(Guid id);
        #endregion


    }
}
