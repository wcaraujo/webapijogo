﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Configs;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Base;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Repositorys
{
    public class EmprestimoDataStore : BaseDataStore<EmprestimoViewModel>, IEmprestimoDataStore
    {


        protected readonly string _endPointEmprestimoGetByJogo = "/getbyjogo";
        protected readonly string _endPointEmprestimoGetByPessoa = "/getbypessoa";
        protected readonly string _endPointEmprestimoGetByPessoaJogo = "/getbypessoajogo";
        protected readonly string _endPointEmprestimoPutFinalizarEmprestimo = "/FinalizarEmprestimo";

        public EmprestimoDataStore(IOptions<AppSetting> options, IHttpContextAccessor httpContextAccessor) : base(options, httpContextAccessor)
        {
        }



        #region methods opcionais
        public async Task<List<EmprestimoViewModel>> GetByJogo(int JogoId)
        {

            var response = await _httpClient.GetAsync($"{_endPointEmprestimo}{_endPointEmprestimoGetByJogo}/{JogoId}");
            var result = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<List<EmprestimoViewModel>>(result);
        }

        public async Task<IEnumerable<EmprestimoViewModel>> GetByPessoa(int PessoaId)
        {

            var response = await _httpClient.GetAsync($"{_endPointEmprestimo}{_endPointEmprestimoGetByPessoa}/{PessoaId}");
            var result = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<List<EmprestimoViewModel>>(result);
        }

        public async Task<EmprestimoViewModel> GetByPessoaJogo(int PessoaId, int JogoId)
        {

            var response = await _httpClient.GetAsync($"{_endPointEmprestimo}{_endPointEmprestimoGetByPessoaJogo}/{PessoaId}/{JogoId}");
            var result = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<EmprestimoViewModel>(result);
        }


        public async Task<bool> FinalizarEmprestimo(Guid id)
        {
            var response = await _httpClient.PutAsync($"{_endPointEmprestimo}{_endPointEmprestimoPutFinalizarEmprestimo}/{id}", null);
            return response.IsSuccessStatusCode;

        }
        #endregion
    }
}
