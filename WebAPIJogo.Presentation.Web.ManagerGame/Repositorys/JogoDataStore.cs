﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using WebAPIJogo.Presentation.Web.ManagerGame.Configs;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Base;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Repositorys
{
    public class JogoDataStore : BaseDataStore<JogoViewModel>, IJogoDataStore
    {
        public JogoDataStore(IOptions<AppSetting> options, IHttpContextAccessor httpContextAccessor) : base(options, httpContextAccessor)
        {
        }
    }
}
