﻿using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Models;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Services
{
    public class AccountDataStoreService : IAccountDataStoreService
    {
        private IAccountDataStore _accountDataStore;

        public AccountDataStoreService(IAccountDataStore accountDataStore)
        {
            _accountDataStore = accountDataStore;
        }

        public Task<Token> SignIn(string username, string password)
        {
            return _accountDataStore.SignIn(username, password);
        }
    }


}
