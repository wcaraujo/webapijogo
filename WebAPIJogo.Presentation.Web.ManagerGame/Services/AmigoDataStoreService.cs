﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Services
{
    public class AmigoDataStoreService : IAmigoDataStoreService
    {
        private IAmigoDataStore _amigoDataStore;

        public AmigoDataStoreService(IAmigoDataStore amigoDataStore)
        {
            _amigoDataStore = amigoDataStore;
        }

        public Task<bool> Create(AmigoViewModel obj)
        {
            return _amigoDataStore.Create(obj);
        }

        public Task<bool> Delete(object key)
        {
            return _amigoDataStore.Delete(key);
        }

        public Task<AmigoViewModel> FindByKeyAsync(object key)
        {
            return _amigoDataStore.FindByKeyAsync(key);
        }

        public Task<List<AmigoViewModel>> GetAllAsync()
        {
            return _amigoDataStore.GetAllAsync();
        }

        public Task<AmigoViewModel> GetEmprestimoPendente(int key)
        {
            return _amigoDataStore.GetEmprestimoPendente(key);
        }

        public Task<bool> Update(object key, AmigoViewModel obj)
        {
            return _amigoDataStore.Update(key, obj);
        }
    }


}
