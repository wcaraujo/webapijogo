﻿using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts
{
    public interface IWrapperDataStore
    {
        IJogoDataStoreService JogoDataStoreService { get; }
        IAmigoDataStoreService AmigoDataStoreService { get; }
        IEmprestimoDataStoreService EmprestimoDataStoreService { get; }
        IAccountDataStoreService AccountDataStoreService { get; }
    }
}
