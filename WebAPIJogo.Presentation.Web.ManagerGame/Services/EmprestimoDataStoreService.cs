﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Services
{
    public class EmprestimoDataStoreService : IEmprestimoDataStoreService
    {
        private IEmprestimoDataStore _emprestimoDataStore;

        public EmprestimoDataStoreService(IEmprestimoDataStore emprestimoDataStore)
        {
            _emprestimoDataStore = emprestimoDataStore;
        }

        public Task<bool> Create(EmprestimoViewModel obj)
        {
            return _emprestimoDataStore.Create(obj);
        }

        public Task<bool> Delete(object key)
        {
            return _emprestimoDataStore.Delete(key);
        }

        public async Task<bool> FinalizarEmprestimo(Guid id)
        {
            return await _emprestimoDataStore.FinalizarEmprestimo(id);
        }

        public Task<EmprestimoViewModel> FindByKeyAsync(object key)
        {
            return _emprestimoDataStore.FindByKeyAsync(key);
        }

        public Task<List<EmprestimoViewModel>> GetAllAsync()
        {
            return _emprestimoDataStore.GetAllAsync();
        }

        public Task<List<EmprestimoViewModel>> GetByJogo(int JogoId)
        {
            return _emprestimoDataStore.GetByJogo(JogoId);
        }

        public Task<IEnumerable<EmprestimoViewModel>> GetByPessoa(int PessoaId)
        {
            return _emprestimoDataStore.GetByPessoa(PessoaId);
        }

        public Task<EmprestimoViewModel> GetByPessoaJogo(int PessoaId, int JogoId)
        {
            return _emprestimoDataStore.GetByPessoaJogo(PessoaId, JogoId);
        }

        public Task<bool> Update(object key, EmprestimoViewModel obj)
        {
            return _emprestimoDataStore.Update(key, obj);
        }
    }


}
