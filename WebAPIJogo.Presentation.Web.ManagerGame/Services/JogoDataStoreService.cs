﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.ViewModels;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Services
{
    public class JogoDataStoreService : IJogoDataStoreService
    {
        private IJogoDataStore _amigoDataStore;

        public JogoDataStoreService(IJogoDataStore amigoDataStore)
        {
            _amigoDataStore = amigoDataStore;
        }

        public Task<bool> Create(JogoViewModel obj)
        {
            return _amigoDataStore.Create(obj);
        }

        public Task<bool> Delete(object key)
        {
            return _amigoDataStore.Delete(key);
        }

        public Task<JogoViewModel> FindByKeyAsync(object key)
        {
            return _amigoDataStore.FindByKeyAsync(key);
        }

        public Task<List<JogoViewModel>> GetAllAsync()
        {
            return _amigoDataStore.GetAllAsync();
        }

        public Task<bool> Update(object key, JogoViewModel obj)
        {
            return _amigoDataStore.Update(key, obj);
        }
    }
}
