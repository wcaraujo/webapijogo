﻿using WebAPIJogo.Presentation.Web.ManagerGame.Repositorys.Contracts;
using WebAPIJogo.Presentation.Web.ManagerGame.Services.Contracts;

namespace WebAPIJogo.Presentation.Web.ManagerGame.Services
{
    public class WrapperDataStore : IWrapperDataStore
    {
        private IJogoDataStoreService jogoDataStoreService;
        private IAmigoDataStoreService amigoDataStoreService;
        private IEmprestimoDataStoreService emprestimoDataStoreService;


        private IAccountDataStoreService accountDataStoreService;


        private IJogoDataStore _jogoDataStore;
        private IAmigoDataStore _amigoDataStore;
        private IEmprestimoDataStore _emprestimoDataStore;
        private IAccountDataStore _accountDataStore;


        public WrapperDataStore(IAccountDataStore accountDataStore, IJogoDataStore jogoDataStore, IAmigoDataStore amigoDataStore, IEmprestimoDataStore emprestimoDataStore)
        {
            this._accountDataStore = accountDataStore;
            _jogoDataStore = jogoDataStore;
            _amigoDataStore = amigoDataStore;
            _emprestimoDataStore = emprestimoDataStore;
        }

        public IJogoDataStoreService JogoDataStoreService
        {
            get
            {
                if (jogoDataStoreService == null)
                {
                    jogoDataStoreService = new JogoDataStoreService(_jogoDataStore);
                }
                return jogoDataStoreService;
            }
        }
        public IAmigoDataStoreService AmigoDataStoreService
        {
            get
            {
                if (amigoDataStoreService == null)
                {
                    amigoDataStoreService = new AmigoDataStoreService(_amigoDataStore);
                }
                return amigoDataStoreService;
            }
        }
        public IEmprestimoDataStoreService EmprestimoDataStoreService
        {
            get
            {
                if (emprestimoDataStoreService == null)
                {
                    emprestimoDataStoreService = new EmprestimoDataStoreService(_emprestimoDataStore);
                }
                return emprestimoDataStoreService;
            }
        }
        public IAccountDataStoreService AccountDataStoreService
        {
            get
            {
                if (accountDataStoreService == null)
                {
                    accountDataStoreService = new AccountDataStoreService(_accountDataStore);
                }
                return accountDataStoreService;
            }
        }
    }
}
