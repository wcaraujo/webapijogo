﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebAPIJogo.Presentation.Web.ManagerGame.ViewModels
{
    public class AmigoViewModel
    {
        #region property
        public int? Id { get; set; }
        [Required]  
        public string Nome { get; set; }
        public virtual List<EmprestimoViewModel> EmprestimosDTO { get; set; } = new List<EmprestimoViewModel>();
        #endregion
    }
}
