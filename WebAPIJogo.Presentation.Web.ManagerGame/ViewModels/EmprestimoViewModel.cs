﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebAPIJogo.Presentation.Web.ManagerGame.ViewModels
{
    public class EmprestimoViewModel
    {



        #region property
        public Guid Id { get; set; }

        public AmigoViewModel AmigoDTO { get; set; }    
        public JogoViewModel JogoDTO { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        public DateTime DataEmprestimo { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        public DateTime DataDevolucao { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        public DateTime DataEntrega { get; set; }

        public bool Entregue { get; set; }
        #endregion
    }
}
