﻿using System.ComponentModel.DataAnnotations;

namespace WebAPIJogo.Presentation.Web.ManagerGame.ViewModels
{
    public class JogoViewModel
    {
        #region property
        public int? Id { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Categoria { get; set; }
        [Required] 
        public bool Ativo { get; set; }
        [Required]
        public bool Disponivel { get; set; }
        #endregion
    }
}
