﻿// =============================
// Invillia
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System;
using WebAPIJogo.Domain.Core.Interfaces.Services;
using WebAPIJogo.Service.Services.Base;

namespace WebAPIJogo.Service.Services
{
    public class ServiceJogo: ServiceBase<Jogo>,IServiceJogo
    {
        #region Constructor
        public ServiceJogo()
        {

        }
        #endregion


        #region Fields
        private readonly IRepositoryBase<T>
        #endregion


        #region property
        #endregion


        #region methods

        /// <summary>
        /// Invoked when the Log In button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void LoginClicked(object obj)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
