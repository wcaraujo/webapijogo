﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;
using System.Net.Http;
using WebAPIJogo.Presentation.API.GerJogo;


namespace WebAPIJogo.Test.WebAPI.Fixtures
{
    public class TestContext
    {
        public HttpClient Client { get; set; }
        private TestServer _server;
        public TestContext()
        {
            SetupClient();
        }
        private void SetupClient()
        {
            _server = new TestServer(new WebHostBuilder()
                .ConfigureServices(ser => { ser.AddAutofac(); })
                .UseStartup<Startup>());
            Client = _server.CreateClient();
        }
    }
}
