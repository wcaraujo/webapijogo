﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace WebAPIJogo.Test.WebAPI.Models
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class Token
    {
        [DataMember(Name = "access_token")]
        public string Access_token { get; set; }

    }
}
