﻿using System.Net;
using System.Threading.Tasks;
using WebAPIJogo.Test.WebAPI.Fixtures;
using Xunit;
using FluentAssertions;
using System.Net.Http.Headers;
using WebAPIJogo.Test.WebAPI.Services;

namespace WebAPIJogo.Test.WebAPI.Scenarios
{

    public class EmprestimoControllerTest
    {
       

        private readonly TestContext _testContext;

        public EmprestimoControllerTest()
        {
            _testContext = new TestContext();
            var token = AccountService.SignIn("batman", "batman").Result;
            _testContext.Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _testContext.Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Access_token);

        }

        [Fact]
        public async Task Get_ALL_ReturnsOkResponse()
        {
            // Act
            var response = await _testContext.Client.GetAsync("/api/emprestimo");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task Get_GetById_ReturnsOkResponse()
        {
            var response = await _testContext.Client.GetAsync("/api/emprestimo/92708320-590e-4d97-8bf6-08d8824fe4ac");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Get_ById_ReturnsBadRequestResponse()
        {
            var response = await _testContext.Client.GetAsync("/api/emprestimo/xxx");
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Get_ById_CorrectContentType()
        {
            var response = await _testContext.Client.GetAsync("/api/emprestimo/92708320-590e-4d97-8bf6-08d8824fe4ac");
            response.EnsureSuccessStatusCode();
            response.Content.Headers.ContentType.ToString().Should().Be("application/json; charset=utf-8");
        }

    }
}
