﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WebAPIJogo.Test.WebAPI.Fixtures;
using WebAPIJogo.Test.WebAPI.Services;
using Xunit;

namespace WebAPIJogo.Test.WebAPI.Scenarios
{
    public class PessoaControllerTest
    {
        private readonly TestContext _testContext;

        public PessoaControllerTest()
        {
            _testContext = new TestContext();
            var token = AccountService.SignIn("batman", "batman").Result;
            _testContext.Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _testContext.Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Access_token);
        }

        [Fact]
        public async Task Get_ALL_ReturnsOkResponse()
        {
            // Act
            var response = await _testContext.Client.GetAsync("/api/pessoa");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task Get_GetById_ReturnsOkResponse()
        {
            var response = await _testContext.Client.GetAsync("/api/pessoa/1");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Get_ById_ReturnsBadRequestResponse()
        {
            var response = await _testContext.Client.GetAsync("/api/pessoa/xxx");
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Get_ById_CorrectContentType()
        {
            var response = await _testContext.Client.GetAsync("/api/pessoa/1");
            response.EnsureSuccessStatusCode();
            response.Content.Headers.ContentType.ToString().Should().Be("application/json; charset=utf-8");
        }
    }
}
