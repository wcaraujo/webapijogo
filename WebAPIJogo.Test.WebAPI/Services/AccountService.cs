﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebAPIJogo.Test.WebAPI.Models;

namespace WebAPIJogo.Test.WebAPI.Services
{
    public static class AccountService
    {
        public static async Task<Token> SignIn(string username, string password)
        {
            HttpClient _httpClient = new HttpClient() { BaseAddress = new Uri("https://localhost:44302/") };


            User userAuthLoginViewModel = new User() { Username = username, Password = password };
            var serializedItem = JsonConvert.SerializeObject(userAuthLoginViewModel);
            var response = await _httpClient.PostAsync("api/account/signIn", new StringContent(serializedItem, Encoding.UTF8, "application/json"));
            var result = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
                return null;
            else
                return JsonConvert.DeserializeObject<Token>(result);
        }
    }
}
